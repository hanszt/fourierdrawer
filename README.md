# Fourier Drawer Complex

---

A program to draw an arbitrary input signal using the Fourier transform.

Made By Hans Zuidervaart.

Thanks to The Coding Train and 3Blue1Brown

---

## About

This program visualizes the workings of the Fourier Transform.

Bellow are snapshots of what the program looks like.

example 1: Image of a single line drawing defined in tow x, y arrays
![picture](src/main/resources/image/example1.png)

example 2: Scanned in image with dotted picture
![picture](src/main/resources/image/example2.png)

example 3: Zoom in active arms
![picture](src/main/resources/image/example3.png)

example 4: Zoom in active arms
![picture](src/main/resources/image/example4.png)

example 5:
![picture](src/main/resources/image/example5.png)

In mathematics, the discrete Fourier transform (DFT) converts a finite sequence of equally-spaced samples of a function
into a same-length sequence of equally-spaced samples of the discrete-time Fourier transform (DFT), which is a
complex-valued function of frequency. The interval at which the DFT is sampled is the reciprocal of the duration of the
input sequence. An inverse DFT is a Fourier series, using the DFT samples as coefficients of complex sinusoid's at the
corresponding DFT frequencies. It has the same sample-values as the original input sequence. The DFT is therefore said
to be a frequency domain representation of the original input sequence. If the original sequence spans all the non-zero
values of a function, its DFT is continuous (and periodic), and the DFT provides discrete samples of one cycle. If the
original sequence is one cycle of a periodic function, the DFT provides all the non-zero values of one DFT cycle.

---

## Motivation

The fourier transform is widely used in all kinds of fields. Think of sound editing, light analysis or analysis of any
other wave fenomenen. I wanted to make a program that visualizes the workings of the fourier transform.

With the fourier transform, any function can be converted to a sum of sine and cosine waves which makes for example
differential computation of any arbitrary function much more manageable.

---

## Definition

The discrete Fourier transform, transforms a sequence of N complex numbers { points n } := points 0 , points 1 , …
points N − 1
into another sequence of complex numbers, { X k } := X 0 , X 1 , … , X N − 1 , which is defined by

![picture](src/main/resources/documentation/FourierTransform.png)

    X k = ∑ n = 0 N − 1 points n ⋅ e − id 2 π N k n = ∑ n = 0 N − 1 points n ⋅ [ cos ⁡ ( 2 π k n / N ) − id ⋅ sin ⁡ ( 2 π k n / N ) ]
    (Eq.1)

where the last expression follows from the first one by Euler's formula.

---

### Fast fourier transform (Fft)

The fast fourier transform is an optimization of the discrete fourier transform. It brings down the time complexity from
O(n^2) to O(n * log(n)).

This has huge implications. Without the Fft, data compression, signal transmission systems like Wi-Fi would not be
possible.

In this project, both the dft and the fft are implemented.

---

## Input

You can also add new input arrays of your own off course. The following tool can be useful for that:
[mobile fish](https://www.mobilefish.com/services/record_mouse_coordinates/record_mouse_coordinates.php)

---

## Tests

Some tests make use of TestFx

To let TestFX manage Mouse and Keyboard on macOS, you have to authorized it.

* Go to System Settings > Privacy & Security > Accessibility
    * If you want to execute the tests from your IDE, check your IDE in the list
    * If you want to execute the tests from maven/gradle in your terminal, check the Terminal
    * If you don't find the Terminal or your IDE, click on + and add it to the list
        * Terminal should be there /Applications/Utilities/Terminal.app

See also [Fix with documentation](https://github.com/bjalon/TestFX/commit/13cdd07e5e6091a5f067edc1a80b8b126de3fd0b)

---

### Sources

- [Wikipedia: Discrete Fourier transform](https://en.wikipedia.org/wiki/Discrete_Fourier_transform)
- [But what is a Fourier series?](https://www.youtube.com/watch?v=r6sGWTCMz2k&t)
- [Coding Challenge #130.3: Fourier Transform Drawing with Complex Number Input](https://www.youtube.com/watch?v=7_vKzcgpfvU)
- [The Fast Fourier Transform Algorithm](https://www.youtube.com/watch?v=toj_IoCQE-4)
- [Fft in java](https://introcs.cs.princeton.edu/java/97data/FFT.java.html)
- [record mouse coordinates](https://www.mobilefish.com/services/record_mouse_coordinates/record_mouse_coordinates.php)
- [Testing JavaFX applications with TestFX: an introduction](http://torgen-engineering.blogspot.com/2015/11/testing-javafx-applications-with-testfx.html)

---
