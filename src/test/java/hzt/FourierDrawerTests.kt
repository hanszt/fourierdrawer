package hzt

import hzt.controller.AppManager
import hzt.model.data.ArmType
import hzt.model.data.InputGenerator.fillInputList
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.shouldBe
import javafx.scene.control.ColorPicker
import javafx.scene.control.Slider
import javafx.scene.input.KeyCode
import javafx.stage.Stage
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.api.FxRobot
import org.testfx.framework.junit5.ApplicationExtension
import org.testfx.framework.junit5.Start

@ExtendWith(ApplicationExtension::class)
class FourierDrawerTests {

    @Start
    private fun start(stage: Stage) {
        fillInputList()
        AppManager().setupStage(stage)
    }

    @Test
    fun `when app starts up, controls are discoverable and controllable`(robot: FxRobot) = testWith(robot) {
        val backgroundColorPicker = robot.lookup("#bg-color-picker").queryAs<ColorPicker>()
        clickOn(backgroundColorPicker).clickOn().type(KeyCode.TAB).type(KeyCode.TAB).type(KeyCode.ENTER)

        val armPartCountSlider = lookup("#armPartCountSlider").queryAs<Slider>()

        drag(armPartCountSlider).interact { armPartCountSlider.value = 4.0 }
        drag(armPartCountSlider).interact { armPartCountSlider.value = 100.0 }

        val armTypeComboBox = lookup("#dftConverterOptions").queryComboBox<ArmType>()
        val initialArmType = armTypeComboBox.value.name
        clickOn(armTypeComboBox).clickOn().type(KeyCode.DOWN).type(KeyCode.DOWN).type(KeyCode.DOWN)
        val armTypeAfterSelection = armTypeComboBox.value.name

        val colorPickers = lookup<ColorPicker> { true }.queryAll<ColorPicker>()
        val colorPicker = colorPickers.first()
        clickOn(colorPicker).clickOn().type(KeyCode.TAB).type(KeyCode.TAB).type(KeyCode.ENTER)

        val slider = lookup(Slider::isShowTickMarks).queryAll<Slider>().first()

        drag(slider).interact { slider.value = 10.0 }
        drag(slider).interact { slider.value = 100.0 }

        assertSoftly {
            colorPickers.size shouldBe 6
            armTypeAfterSelection shouldBe "4. Double Arm"
            initialArmType shouldBe "3. Single Arm (Fast Fourier)"
        }
    }
}
