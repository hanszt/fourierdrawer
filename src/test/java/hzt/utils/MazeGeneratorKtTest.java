package hzt.utils;

import hzt.model.data.InputGenerator;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MazeGeneratorKtTest {

    @Test
    void testArrayMatchesCopy() {
        //given
        int[] generatedArray = InputGenerator.getRandomDiscreetValueSignal(4, 200);
        System.out.println("generatedArray.length = " + generatedArray.length);
        final var copy = Arrays.copyOf(generatedArray, generatedArray.length);
        //when
        assertArrayEquals(generatedArray, copy);
    }

    @Test
    void mazeMustHaveOddDimensions() {
        final var exception = assertThrows(IllegalArgumentException.class,
                () -> MazeGeneratorKt.generateRandomPerfectMaze(new Dimension(30, 20)));
        assertEquals("width (30) and height (20) must be odd", exception.getMessage());
    }

    @Test
    void generateRandomPerfectMaze() {
        final List<Cell> cells = MazeGeneratorKt
                .generatePerfectMaze(new Dimension(31, 21),
                        dimension -> new GridPoint(1, 1),
                        list -> list.get(0));

        assertAll(
                () -> assertEquals(150, cells.size())
        );
    }
}
