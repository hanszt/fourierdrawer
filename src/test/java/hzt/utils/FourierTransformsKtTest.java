package hzt.utils;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;
import org.apache.commons.math3.util.Precision;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.function.IntFunction;
import java.util.stream.Stream;

import static hzt.utils.FourierTransformsKt.fft;
import static hzt.utils.FourierUtilsKtTestKt.HIGHEST_POWER_OF_TWO_INT;
import static org.junit.jupiter.api.Assertions.*;

class FourierTransformsKtTest {

    @NotNull
    private static final IntFunction<Complex[]> createInput = maxSize ->
            Stream.iterate(Complex.ZERO, n -> n.add(new Complex(.01, .02)))
                    .limit(maxSize)
                    .toArray(Complex[]::new);

    @Test
    void testOwnImplementationOfFftAgainstApacheCommonsVersion() {
        final var input = createInput.apply(FourierUtilsKt.nextPowerOfTwo(10_000));

        final var transform = new FastFourierTransformer(DftNormalization.STANDARD)
                .transform(input, TransformType.FORWARD);

        final var result = fft(input);

        final var expected = roundValuesInComplexNrArray(transform);
        final var actual = roundValuesInComplexNrArray(result);

        assertArrayEquals(expected, actual);
    }

    @Test
    @Timeout(5)
    void testOwnFftLargePowerOfTwoSize() {
        final int size = HIGHEST_POWER_OF_TWO_INT / 512;
        System.out.println("size = " + size);
        final var input = new Complex[size];
        Arrays.fill(input, Complex.ONE);
        final Complex[] result = fft(input);
        assertEquals(result.length, size);
    }

    @Test
    @Timeout(5)
    void testApacheCommonsFftLargePowerOfTwoSize() {
        final int size = HIGHEST_POWER_OF_TWO_INT / 128;
        System.out.println("size = " + size);
        final var input = new Complex[size];
        Arrays.fill(input, Complex.ONE);
        final Complex[] result = new FastFourierTransformer(DftNormalization.STANDARD)
                .transform(input, TransformType.FORWARD);
        assertEquals(result.length, size);
    }

    @ParameterizedTest(name = "when input length is not a power of 2 (size = {0}), then throw")
    @ValueSource(ints = {3, 5, 7, 10, 12, 1000})
    void testThrowsExceptionWhenInputSizeNotPowerOfTwo(int size) {
        assertThrows(IllegalArgumentException.class, () -> fft(new Complex[size]));
    }

    @Test
    void testComparingFftAndDft() {
        Complex[] input = createInput.apply(256);

        final Complex[] dft = FourierTransformsKt.dft(input);

        final Complex[] fft = new FastFourierTransformer(DftNormalization.STANDARD)
                .transform(input, TransformType.FORWARD);

        final var roundedResult = roundValuesInComplexNrArray(fft);
        final var roundedResult1 = roundValuesInComplexNrArray(dft);

        System.out.println("fft = " + Arrays.toString(roundedResult));
        System.out.println("dft = " + Arrays.toString(roundedResult1));

        assertArrayEquals(roundedResult, roundedResult1);
    }

    @Test
    void testInverseFft() {
        final Complex[] input = createInput.apply(256);

        final Complex[] fft = FourierTransformsKt.fft(input);

        final var inverse = FourierTransformsKt.inverseFft(fft);

        final var expected = roundValuesInComplexNrArray(input);
        final var actual = roundValuesInComplexNrArray(inverse);

        assertArrayEquals(expected, actual);
    }

    @Test
    void testConvolve() {
        final Complex[] input1 = createInput.apply(258);
        final Complex[] input2 = createInput.apply(275);
        final var convolution = FourierTransformsKt.convolve(input1, input2);

        assertEquals(512, convolution.length);
    }

    @NotNull
    private Complex[] roundValuesInComplexNrArray(Complex[] complexes) {
        return Arrays.stream(complexes)
                .map(nr -> new Complex(round(nr.getReal()), round(nr.getImaginary())))
                .toArray(Complex[]::new);
    }

    private static double round(double complex) {
        final var scale = 4;
        final var rounded = Precision.round(complex, scale);
        if (Double.compare(-0, rounded) == 0) {
            return rounded * -1;
        }
        return rounded;
    }

}
