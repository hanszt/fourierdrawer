package hzt.utils;

import hzt.model.EpiCycleSort;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.util.ArithmeticUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SignalConverterKtTest {

    @Test
    void testDuplicatedToPowerOfTwoLength() {
        int[] inputArray = {2, 5, 6, 7, 4};
        int[] expected = {2, 2, 5, 5, 6, 6, 7, 4};
        int[] result = SignalConverterKt.duplicateIfSizeNotPowerOfTwo(inputArray);
        System.out.println("Arrays.toString(expected) = " + Arrays.toString(result));
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    void testIsSameWhenLengthPowerOfTwo() {
        int[] inputArray = {2, 5, 6, 7};
        int[] result = SignalConverterKt.duplicateIfSizeNotPowerOfTwo(inputArray);
        assertEquals(inputArray.length, result.length);
    }

    @Test
    void testPadToPowerOfTwo() {
        int[] array = {1, 4, 3, 5, 2, 6, 4, 4, 3};
        final var ints = SignalConverterKt.duplicateIfSizeNotPowerOfTwo(array);

        System.out.println(Arrays.toString(ints));

        assertTrue(ArithmeticUtils.isPowerOfTwo(ints.length));
    }

    @Test
    void testAreEpiCycleListsEqualFastFourierTransformAndDft() {
        Complex[] input = createInputLine();

        final var amplitudeComparator = EpiCycleSort.AMPLITUDE;

        final var epiCyclesFft = SignalConverterKt.toEpiCycleListByApacheCommonsFft(input, amplitudeComparator.getComparator());

        final var epiCyclesDft = SignalConverterKt.toEpiCycleListByDft(input, amplitudeComparator.getComparator());

        System.out.println("epiCyclesDft");
        epiCyclesDft.forEach(System.out::println);

        System.out.println("epiCyclesFft");
        epiCyclesFft.forEach(System.out::println);

        assertEquals(epiCyclesDft.size(), epiCyclesFft.size());
    }

    @NotNull
    private Complex[] createInputLine() {
        return Stream.iterate(new Complex(0, 0), n -> n.add(new Complex(1, 1)))
                .limit(8)
                .toArray(Complex[]::new);
    }
}
