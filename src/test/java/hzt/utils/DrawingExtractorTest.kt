package hzt.utils

import hzt.model.image.Image
import hzt.model.image.Pixel
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class DrawingExtractorTest {

    @Test
    fun `pixels from scanned images should be reduced in size and ordered radially`() {
        fun linePixel(x: Int, y: Int) = Pixel(x, y, true)
        val pixels = listOf(
            linePixel(3, 2),
            linePixel(0, 5),
            linePixel(2, 10),
            linePixel(3, 3),
            linePixel(2, 3),
            linePixel(1, 2),
            linePixel(35, 2),
            linePixel(2, 2),
        )
        val sortedPixels = DrawingExtractor.toReducedSortedPixelList(pixels.createImage())
        sortedPixels.forEach(::println)
        assertEquals(
            listOf(
                linePixel(3, 3),
                linePixel(2, 3),
                linePixel(3, 2),
                linePixel(2, 2),
                linePixel(0, 5),
                linePixel(1, 2),
            ), sortedPixels
        )
    }
}

private fun List<Pixel>.createImage(): Image {
    val minX = minOf(Pixel::x)
    val maxX = maxOf(Pixel::x)
    val minY = minOf(Pixel::y)
    val maxY = maxOf(Pixel::y)
    val width = maxX - minX
    val height = maxY - minY
    val map = associate { GridPoint(it.x, it.y) to it }
    val pixels = Array(height) { row ->
        Array(width) { col -> Pixel(col, row, map[GridPoint(col, row)]?.isLine ?: false) }
    }
    return Image("Test", pixels)
}
