package hzt.utils

import org.apache.commons.math3.util.ArithmeticUtils
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

const val HIGHEST_POWER_OF_TWO_INT = 1_073_741_824

internal class FourierUtilsKtTest {

    @ParameterizedTest
    @ValueSource(ints = [2, 4, 8, 16, 32, 128])
    fun `is power of two`(nr: Int) = assertTrue(isPowerOfTwo(nr))

    @ParameterizedTest
    @ValueSource(ints = [10, 5, 6, 18, 33, 123])
    fun `is not a power of two`(nr: Int) = assertFalse(isPowerOfTwo(nr))

    @ParameterizedTest
    @ValueSource(ints = [Int.MIN_VALUE, -10, 1, 12, 234_234, 0, 2343, 234, 23_423, 257, 512, 497_676, 100_000, HIGHEST_POWER_OF_TWO_INT, Int.MAX_VALUE / 2])
    fun testYieldsNextPowerOfTwo(integer: Int) {
        val nextPowerOfTwo = integer.nextPowerOfTwo()
        println("nextPowerOfTwo = $nextPowerOfTwo")
        assertTrue(ArithmeticUtils.isPowerOfTwo(nextPowerOfTwo.toLong()))
    }

    @ParameterizedTest(name = "when the next power of two of {0} is greater than Integer.MAX_VALUE, an illegal argument exception should be thrown")
    @ValueSource(ints = [Int.MAX_VALUE, 1_200_000_000, HIGHEST_POWER_OF_TWO_INT + 1])
    fun testThrowsIllegalArgumentException(value: Int) {
        val exception = assertThrows<IllegalArgumentException> { value.nextPowerOfTwo() }
        assertEquals("No next power of two found for $value", exception.message)
    }
}
