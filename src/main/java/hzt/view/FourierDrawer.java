package hzt.view;

import hzt.controller.AppManager;
import hzt.model.EpiCycleSort;
import hzt.model.data.InputValue;
import hzt.model.fourier_transform.EpiCycle;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.apache.commons.math3.complex.Complex;

import java.util.Comparator;
import java.util.List;

import static hzt.view.AnimationVars.PATH_WIDTH;
import static java.lang.Math.PI;
import static javafx.animation.Animation.Status.RUNNING;

public abstract class FourierDrawer {

    protected AppManager appManager;
    protected AnimationVars animationVars;
    protected UserInputController userInputController;
    protected ColorController colorController;

    FourierDrawer(AppManager appManager,
                  AnimationVars animationVars,
                  UserInputController userInputController,
                  ColorController colorController) {
        this.appManager = appManager;
        this.animationVars = animationVars;
        this.userInputController = userInputController;
        this.colorController = colorController;
    }
    public void drawFourier(int inputSize, GraphicsContext graphics) {
        resetAnimationEveryCycle(inputSize);
        colorController.setChangingColor(userInputController);
        manageAnimation(graphics);
        if (appManager.getTimeline().getStatus() == RUNNING) {
            updateTime(inputSize);
        }
        animationVars.setFrameCount(animationVars.getFrameCount() + 1);
        StatsController.showStats(appManager, userInputController, colorController, graphics);
    }

    private void updateTime(int inputSize) {
        animationVars.time += (appManager.getTimeline().getRate() >= 0 ? (2 * PI) : (-2 * PI)) / inputSize;
    }

    private void manageAnimation(GraphicsContext graphics) {
        clearCanvasForNewFrame(graphics);
        final var specPath = animationVars.specPath;
        final var ofSetOrigin = animationVars.ofSetOrigin;
        if (userInputController.getFollowButton().isActive()) {
            userInputController.updatePositionSliders(-ofSetOrigin.getX(), ofSetOrigin.getY(), animationVars.angle);
            animationVars.ofSetOrigin = !specPath.isEmpty() ?
                    new Point2D(-specPath.get(0).getReal(), -specPath.get(0).getImaginary()) : Point2D.ZERO;
        }
        graphics.translate(animationVars.posOrigin.getX() +ofSetOrigin.getX(),
                animationVars.posOrigin.getY() + ofSetOrigin.getY());
        updateVarsAnimation(graphics);
        if (appManager.getTimeline().getStatus() == RUNNING) {
            removeLastEntry();
        }
        if (userInputController.getSpecPathButton().isActive()) {
            drawPath(specPath, colorController.getSpecPathColor(), graphics);
        }
        if (userInputController.getFinalPathButton().isActive()) {
            drawPath(animationVars.finalPath, colorController.getFinalPathColor(), graphics);
        }
        graphics.translate(-(animationVars.posOrigin.getX() + ofSetOrigin.getX()),
                -(animationVars.posOrigin.getY() + ofSetOrigin.getY()));
    }

    abstract void updateVarsAnimation(GraphicsContext graphics);

    private void clearCanvasForNewFrame(GraphicsContext graphics) {
        final var scene = appManager.getScene();
        double width = scene.getWidth();
        double height = scene.getHeight();
        graphics.clearRect(0, 0, width, height);
    }

    private void drawPath(List<Complex> path, Color color, GraphicsContext gc) {
        double pathWidth = PATH_WIDTH * animationVars.scaleFactor;
        gc.setLineWidth(pathWidth);
        gc.setStroke(color);
        for (int i = 0; i < path.size() - 1; i++) {
            double re = path.get(i).getReal();
            double im = path.get(i).getImaginary();
            if (i != path.size() + animationVars.getFrameCount()) {
                if (!userInputController.getDottedLineButton().isActive()) {
                    double nextRe = path.get(i + 1).getReal();
                    double nextIm = path.get(i + 1).getImaginary();
                    gc.strokeLine(re, im, nextRe, nextIm);
                } else {
                    gc.strokeOval(re, im, pathWidth / 2., pathWidth / 2.);
                }
            }
        }
    }

    private void removeLastEntry() {
        final var maxPathLength = animationVars.getMaxPathLength();
        if (animationVars.finalPath.size() >= maxPathLength) {
            animationVars.finalPath.remove(maxPathLength - 1);
        }
        if (animationVars.specPath.size() >= maxPathLength) {
            animationVars.specPath.remove(maxPathLength - 1);
        }
    }

    private void resetAnimationEveryCycle(int inputSize) {
        if (animationVars.getFrameCount() >= inputSize) {
            animationVars.setCycleCount(animationVars.getCycleCount() + 1);
            animationVars.setFrameCount(0);
        }
    }

    public void initialize(InputValue inputValue, EpiCycleSort epiCycleSort) {
        convertInput(inputValue, epiCycleSort);
        setRefRadius();
        animationVars.setMaxPathLength(inputValue.size());
    }

    protected abstract void setRefRadius();

    public abstract void convertInput(InputValue inputValue, EpiCycleSort epiCycleSort);

    public abstract void sortEpiCycles(Comparator<EpiCycle> comparator);
}
