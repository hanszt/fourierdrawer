package hzt.view;

import hzt.controller.AppManager;
import hzt.controller.DrawModeController;
import hzt.model.EpiCycleSort;
import hzt.model.custom_controls.SwitchButton;
import hzt.model.data.ArmType;
import hzt.model.data.InputGenerator;
import hzt.model.data.InputValue;
import hzt.utils.io.FileUtils;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.apache.commons.math3.complex.Complex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.stream.Collectors;

import static hzt.utils.AppConstants.*;
import static javafx.animation.Animation.Status.PAUSED;

public class UserInputController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInputController.class);
    private static final double MIN_SCALE_FACTOR = 0.05;
    private static final double MAX_SCALE_FACTOR = 10;
    private static final int DIST_FROM_EDGE = 25;
    private static final int PREF_WIDTH_MENU = 240;
    private static final int PREF_HEIGHT_BUTTONS = 20;
    private static final int INIT_V_POS_CONTROLS = 0;
    private static final double OPACITY_VALUE = 0.7;

    private final AppManager appManager;
    private final DrawModeController drawModeController;
    private final ColorController colorController;

    private AnimationVars animationVars;

    private SwitchButton finalPathButton;
    private SwitchButton specPathButton;
    private SwitchButton dottedLineButton;
    private SwitchButton circlesButton;
    private SwitchButton armsButton;
    private SwitchButton showStatsButton;
    private SwitchButton followButton;
    private SwitchButton multiButton;

    private SwitchButton fullScreenButton;
    private SwitchButton transparentButton;
    private SwitchButton pauseButton;

    private Button restart;
    private Button drawButton;
    private Button clearPathsButton;
    private Button printImagesButton;
    private Button deleteButton;
    private Button reCenterButton;
    private Button newInstanceButton;
    private DoubleConsumer brightnessSliderUpdater;
    private DoubleConsumer zoomSliderUpdater;
    private DoubleConsumer horizontalSliderUpdater;
    private DoubleConsumer verticalSliderUpdater;
    private DoubleConsumer rotationSliderUpdater;

    private int vPosControls = INIT_V_POS_CONTROLS;
    private final DoubleProperty controlsOpacity = new SimpleDoubleProperty();

    public UserInputController(AppManager appManager,
                               ColorController colorController,
                               DrawModeController drawModeController) {
        this.appManager = appManager;
        this.colorController = colorController;
        this.drawModeController = drawModeController;
    }

    private void initializeControlsVars() {
        animationVars = appManager.getArmType().drawer().animationVars;
        controlsOpacity.set(OPACITY_VALUE);
    }

    public GridPane buildConfiguredControls() {
        initializeControlsVars();
        GridPane controlsGrid = new GridPane();
        setControlsGridPane(controlsGrid);
        setupLabels(controlsGrid);
        setupDropdownMenus(controlsGrid);
        setupButtons(controlsGrid);
        setupAnimationButtons(controlsGrid);
        setupColorPickers(controlsGrid);
        setupSliders(controlsGrid);
        controlsGrid.getChildren().forEach(this::setGeneralPropertiesControl);
        vPosControls = INIT_V_POS_CONTROLS;
        return controlsGrid;
    }

    private void setGeneralPropertiesControl(Node node) {
        node.setOpacity(controlsOpacity.getValue());
        node.setCursor(Cursor.HAND);
    }

    private void setControlsGridPane(GridPane grid) {
        grid.setHgap(2);
        grid.setVgap(4);
        grid.setAlignment(Pos.BOTTOM_CENTER);
        grid.setPrefWidth(SCREEN_SIZE.getWidth());
        grid.setPrefHeight(SCREEN_SIZE.getHeight());
        double yScaleFactor = appManager.getScene().getHeight() / INIT_SCENE_SIZE.getHeight();
        if (yScaleFactor > 1) {
            yScaleFactor = 1;
        }
        grid.setScaleY(yScaleFactor);
        // height, right, bottom, left
        grid.setPadding(new Insets(DIST_FROM_EDGE, DIST_FROM_EDGE, DIST_FROM_EDGE * 2., DIST_FROM_EDGE));
    }

    private void setupLabels(GridPane grid) {
        Label label1 = new Label();
        Label label2 = new Label();
        label1.setPrefWidth(PREF_WIDTH_MENU);
        label1.setPrefHeight(SCREEN_SIZE.getHeight() / 2.);
        label2.setPrefWidth(SCREEN_SIZE.getWidth() * 2 / 3.);
        grid.add(label1, 0, vPosControls++, 2, 1);
        grid.add(label2, 2, 0, 2, 1);
    }

    private void setupDropdownMenus(GridPane grid) {
        vPosControls++;
        setupInputDataDropdownMenu(grid);
        setupDftDropdownMenu(grid);
        setupSortByDropdownMenu(grid);
    }

    private void setupInputDataDropdownMenu(GridPane grid) {
        final ComboBox<InputValue> inputOptions = setupComboBox(grid, vPosControls++);
        inputOptions.getItems().addAll(InputGenerator.getInputAsSortedList());
        inputOptions.setValue(appManager.getInput());
        inputOptions.setOnAction(e -> loadNewInputData(inputOptions.getValue()));
    }

    private void loadNewInputData(InputValue selected) {
        appManager.getTimeline().stop();
        appManager.reset();
        appManager.selectDftOutputSignal(selected, appManager.getArmType());
        appManager.updateControls();
        appManager.getTimeline().play();
    }

    private void setupDftDropdownMenu(GridPane grid) {
        final ComboBox<ArmType> dftConverterOptions = setupComboBox(grid, vPosControls++);
        dftConverterOptions.setId("dftConverterOptions");
        dftConverterOptions.getItems().addAll(appManager.constructArmTypes());
        dftConverterOptions.setValue(appManager.getArmType());
        dftConverterOptions.setOnAction(e -> convertWithSelectedDft(dftConverterOptions.getValue()));
    }

    private void convertWithSelectedDft(ArmType selected) {
        appManager.reset();
        appManager.setArmType(selected);
        appManager.selectDftOutputSignal(appManager.getInput(), appManager.getArmType());
        appManager.updateControls();
        appManager.updateFrameIfPaused();
    }

    private void setupSortByDropdownMenu(GridPane grid) {
        final ComboBox<EpiCycleSort> sortByOptions = setupComboBox(grid, vPosControls++);
        sortByOptions.getItems().addAll(EnumSet.allOf(EpiCycleSort.class));
        sortByOptions.setValue(appManager.getEpiCycleComparator());
        sortByOptions.setOnAction(e -> sortBySelectedParam(sortByOptions.getValue()));
    }

    private void sortBySelectedParam(EpiCycleSort selected) {
        long start = System.currentTimeMillis();
        appManager.setSortingComparator(selected);
        final var armType = appManager.getArmType();
        armType.drawer().sortEpiCycles(selected.getComparator());
        LOGGER.info("sorted by {} in {} milliseconds.\n", selected.getDescription(), System.currentTimeMillis() - start);
        appManager.updateFrameIfPaused();
    }

    private static  <T> ComboBox<T> setupComboBox(GridPane grid, int vPos) {
        ComboBox<T> comboBox = new ComboBox<>();
        comboBox.setPrefWidth(PREF_WIDTH_MENU);
        comboBox.minWidth(PREF_WIDTH_MENU);
        comboBox.setConverter(buildStringConverter());
        grid.add(comboBox, 0, vPos, 2, 1);
        return comboBox;
    }

    private static <T> StringConverter<T> buildStringConverter() {

        return new StringConverter<>() {
            @Override
            public String toString(T item) {
                final String result;
                if (item instanceof EpiCycleSort epiCycleSort) {
                    result =  "Sort by " + epiCycleSort.getDescription();
                } else if (item instanceof ArmType armType) {
                    result =  armType.name();
                } else if (item instanceof InputValue inputValue) {
                    result =  inputValue.name();
                } else {
                    result = item.toString();
                }
                return result;
            }

            @Override
            public T fromString(String id) {
                return null;
            }
        };
    }

    private void setupButtons(GridPane grid) {
        String newInstanceLabel = "new instance";
        restart = setupButton(grid, "restart sim", 0, vPosControls);
        pauseButton = setupSwitchButton(grid, false, "play sim", "pause sim", 1, vPosControls++);
        newInstanceButton = setupButton(grid, newInstanceLabel, 0, vPosControls);
        drawButton = setupButton(grid, "draw something", 1, vPosControls++);
        clearPathsButton = setupButton(grid, "clear paths", 0, vPosControls);
        reCenterButton = setupButton(grid, "recenter", 1, vPosControls++);
        printImagesButton = setupButton(grid, "print output images", 0, vPosControls);
        deleteButton = setupButton(grid, "delete output images", 1, vPosControls++);
        pauseButton.setActiveStyle(
                "-fx-background-color: lightgreen;" +
                        "-fx-text-fill: darkred"/* +
                        "-fx-border-color: darkred"*/);
        addActionEvents();
    }

    private void addActionEvents() {
        restart.setOnAction(e -> appManager.restartSim());
        drawButton.setOnAction(e -> drawModeController.getNewDrawCanvas());
        clearPathsButton.setOnAction(e -> clearPaths());
        printImagesButton.setOnAction(e -> printImage());
        deleteButton.setOnAction(e -> deleteImages());
        reCenterButton.setOnAction(e -> recenter());
        newInstanceButton.setOnAction(e -> createNewInstance());
    }

    private void clearPaths() {
        animationVars.specPath.clear();
        animationVars.finalPath.clear();
        appManager.updateFrameIfPaused();
    }

    public void limitInstances(String label) {
        newInstanceButton.setDisable("Maximum reached".equals(label));
        newInstanceButton.setText(label);
    }

    private static void createNewInstance() {
        AppManager manager = new AppManager();
        manager.setupStage(new Stage());
    }

    private void recenter() {
        followButton.setActive(false);
        updatePositionSliders(0, 0, 0);
    }

    private void deleteImages() {
        final var deletionMessage = FileUtils.deleteOutputDirImages();
        PopUpController.showPopupMessage(deletionMessage, true, appManager.getPrimaryStage());
    }

    private void printImage() {
        final var outputImagesMessage = FileUtils.createOutputImagesOfMazeList(InputGenerator.getImages());
        PopUpController.showPopupMessage(outputImagesMessage, false, appManager.getPrimaryStage());
    }

    private void setupAnimationButtons(GridPane grid) {
        vPosControls++;
        fullScreenButton = setupSwitchButton(grid, false, "exit full screen", "full screen", 0, vPosControls);
        showStatsButton = setupSwitchButton(grid, false, "hide stats", "show stats", 1, vPosControls++);
        armsButton = setupSwitchButton(grid, true, "hide arms", "show arms", 0, vPosControls);
        circlesButton = setupSwitchButton(grid, false, "hide circles", "show circles", 1, vPosControls++);
        specPathButton = setupSwitchButton(grid, true, "hide specific path", "show specific path", 0, vPosControls);
        finalPathButton = setupSwitchButton(grid, false, "hide end path", "show end path", 1, vPosControls++);
        dottedLineButton = setupSwitchButton(grid, false, "show line trace", "show dot trace", 0, vPosControls);
        transparentButton = setupSwitchButton(grid, areControlsTransparent(), "solid", "transparent", 1, vPosControls++);
        multiButton = setupSwitchButton(grid, false, "constant colors", "changing colors", 0, vPosControls);
        followButton = setupSwitchButton(grid, false, "fixed camera", "follow draw spot", 1, vPosControls++);
        addActionEventsToSwitchButtons(grid);
        vPosControls++;
    }

    private boolean areControlsTransparent() {
        return Double.compare(controlsOpacity.get(), OPACITY_VALUE) == 0;
    }

    private void addActionEventsToSwitchButtons(GridPane grid) {
        pauseButton.getMultipleEventsHandler().addEventHandler(0, e -> pauseButtonAction());
        fullScreenButton.getMultipleEventsHandler().addEventHandler(e ->
                appManager.getPrimaryStage().setFullScreen(fullScreenButton.isActive()));
        transparentButton.getMultipleEventsHandler().addEventHandler(0, e -> transparentButtonAction(grid));
        multiButton.getMultipleEventsHandler().addEventHandler(e -> multiButtonAction());
    }

    private void multiButtonAction() {
        if (pauseButton.isActive()) {
            appManager.updateFrameIfPaused();
        }
    }

    private void transparentButtonAction(GridPane grid) {
        controlsOpacity.set(areControlsTransparent() ? 1 : OPACITY_VALUE);
        grid.getChildren().forEach(node -> node.setOpacity(controlsOpacity.getValue()));
    }

    private void pauseButtonAction() {
        if (pauseButton.isActive()) {
            appManager.getTimeline().play();
            multiButton.setSwitchModeEnabled(true);
        } else {
            appManager.getTimeline().pause();
            multiButton.setSwitchModeEnabled(false);
            multiButton.setText("update frame");
        }
        boolean simPaused = appManager.getTimeline().getStatus() == PAUSED;
        pauseButton.setButtonFocused(simPaused);
    }

    private static Button setupButton(GridPane grid, String text, int hPos, int vPos) {
        Button button = new Button(text);
        button.setPrefWidth(PREF_WIDTH_MENU / 2.);
        button.setMinWidth(PREF_WIDTH_MENU / 2.);
        button.setPrefHeight(PREF_HEIGHT_BUTTONS);
        grid.add(button, hPos, vPos);
        return button;
    }

    private SwitchButton setupSwitchButton(GridPane grid, boolean on, String onText, String offText, int hPos, int vPos) {
        SwitchButton switchButton = new SwitchButton(on, onText, offText);
        switchButton.setPrefWidth(PREF_WIDTH_MENU / 2.);
        switchButton.setMinWidth(PREF_WIDTH_MENU / 2.);
        switchButton.setPrefHeight(PREF_HEIGHT_BUTTONS);
        switchButton.getMultipleEventsHandler().addEventHandler(e -> appManager.updateFrameIfPaused());
        grid.add(switchButton, hPos, vPos);
        return switchButton;
    }

    private void setupColorPickers(GridPane grid) {
        setupDrawingColorPickers(grid);
        setupOtherColorPickers(grid);
    }

    private void setupDrawingColorPickers(GridPane grid) {
        ColorPicker armBeginColorPicker = setupColorPicker(colorController.getArmBegin());
        ColorPicker armEndColorPicker = setupColorPicker(colorController.getArmEnd());
        armBeginColorPicker.valueProperty().bindBidirectional(colorController.armBeginProperty());
        armEndColorPicker.valueProperty().bindBidirectional(colorController.armEndProperty());
        grid.add(armBeginColorPicker, 0, vPosControls);
        grid.add(armEndColorPicker, 1, vPosControls++);
    }

    private void setupOtherColorPickers(GridPane grid) {
        ColorPicker finalPathColorPicker = setupColorPicker(colorController.getFinalPathColor());
        ColorPicker circleColorPicker = setupColorPicker(colorController.getCircleColor());
        ColorPicker backgroundColorPicker = setupColorPicker(colorController.getBackgroundColor());
        backgroundColorPicker.setId("bg-color-picker");
        ColorPicker specificPathColorPicker = setupColorPicker(colorController.getSpecPathColor());
        specificPathColorPicker.valueProperty().bindBidirectional(colorController.specPathColorProperty());
        finalPathColorPicker.valueProperty().bindBidirectional(colorController.finalPathColorProperty());
        circleColorPicker.valueProperty().bindBidirectional(colorController.circleColorProperty());
        backgroundColorPicker.valueProperty().bindBidirectional(colorController.backgroundColorProperty());
        colorController.backgroundColorProperty().addListener(forNewValue(this::updateBackgroundColor));
        grid.add(specificPathColorPicker, 0, vPosControls);
        grid.add(finalPathColorPicker, 1, vPosControls++);
        grid.add(circleColorPicker, 0, vPosControls);
        grid.add(backgroundColorPicker, 1, vPosControls);
    }

    private void updateBackgroundColor(Color newBackgroundColor) {
        appManager.getRoot().setBackground(new Background(new BackgroundFill(newBackgroundColor, CornerRadii.EMPTY, Insets.EMPTY)));
        brightnessSliderUpdater.accept(newBackgroundColor.getBrightness());
    }

    private static ColorPicker setupColorPicker(Color startColor) {
        ColorPicker colorPicker = new ColorPicker(startColor);
        colorPicker.setPrefWidth(PREF_WIDTH_MENU / 2.);
        colorPicker.prefHeight(PREF_HEIGHT_BUTTONS);
        colorPicker.maxHeight(PREF_HEIGHT_BUTTONS);
        colorPicker.setStyle("-fx-color-label-visible: false ;");
        return colorPicker;
    }

    public void updatePositionSliders(double x, double y, double angle) {
        horizontalSliderUpdater.accept(x);
        verticalSliderUpdater.accept(y);
        rotationSliderUpdater.accept(angle);
    }

    private record HorSliderInput(double minValue, double maxValue, double curValue, int hPos, int vPos, int hSpan) {

    }

    private static <T> ChangeListener<T> forNewValue(Consumer<T> consumer) {
        return (o, c, n) -> consumer.accept(n);
    }

    private void setupSliders(GridPane grid) {
        vPosControls = vPosControls - 3;
        double frameRate = appManager.getTimeline().getRate() * INIT_FRAME_RATE;
        Slider simSpeed = setupHorizontalSlider(grid, new HorSliderInput(-500., 500., frameRate, 2, vPosControls, 1), true);
        Slider armPartCountSlider = setupHorizontalSlider(
                grid, new HorSliderInput(1, appManager.getInput().size(), animationVars.selectedArmPartIndex, 3, vPosControls++, 1), true);
        armPartCountSlider.setId("armPartCountSlider");
        Slider zoomSlider = setupHorizontalSlider(
                grid, new HorSliderInput(MIN_SCALE_FACTOR, MAX_SCALE_FACTOR, animationVars.scaleFactor, 2, vPosControls, 1), false);
        Slider rotateDrawing = setupHorizontalSlider(
                grid, new HorSliderInput(-Math.PI, Math.PI, animationVars.angle, 3, vPosControls++, 1), false);
        Slider brightnessSlider = setupHorizontalSlider(
                grid, new HorSliderInput(0.01, 1, colorController.getBackgroundColor().getBrightness(), 2, vPosControls, 1), false);
        Slider armThicknessSlider = setupHorizontalSlider
                (grid, new HorSliderInput(1, 20, animationVars.armThickness, 3, vPosControls++, 1), false);
        final var scene = appManager.getScene();
        Slider horizontalOrr = new Slider(
                -scene.getWidth(), scene.getWidth(), animationVars.ofSetOrigin.getX());
        grid.add(horizontalOrr, 2, vPosControls, 2, 1);
        Slider verticalOrr = setupVerSlider(grid);

        simSpeed.valueProperty().addListener(forNewValue(n -> appManager.setTimelineRate(n.doubleValue() / INIT_FRAME_RATE)));
        armPartCountSlider.valueProperty().addListener(forNewValue(this::updateArmPartCount));
        zoomSlider.valueProperty().addListener((oldVal, curVal, newVal) -> zoom(curVal, newVal));
        armThicknessSlider.valueProperty().addListener(forNewValue(this::adjustThickness));
        verticalOrr.valueProperty().addListener(forNewValue(this::adjustVerticalOrientation));
        horizontalOrr.valueProperty().addListener(forNewValue(this::adjustHorOrientation));
        rotateDrawing.valueProperty().addListener((oldVal, curVal, newVal) -> rotate(curVal, newVal));
        brightnessSlider.valueProperty().addListener(forNewValue(this::adjustBrightness));
        horizontalSliderUpdater = horizontalOrr::setValue;
        verticalSliderUpdater = verticalOrr::setValue;
        rotationSliderUpdater = rotateDrawing::setValue;
        brightnessSliderUpdater = brightnessSlider::setValue;
        zoomSliderUpdater = zoomSlider::setValue;
    }

    private void adjustBrightness(Number newVal) {
        Color backgroundColor = colorController.getBackgroundColor();
        Color newBc = Color.hsb(backgroundColor.getHue(), backgroundColor.getSaturation(), newVal.doubleValue());

        appManager.getRoot().setBackground(new Background(new BackgroundFill(newBc, CornerRadii.EMPTY, Insets.EMPTY)));
        colorController.backgroundColorProperty().set(newBc);
    }

    private void rotate(Number curVal, Number newVal) {
        animationVars.angle = newVal.doubleValue();
        rotateVariablesInPathLists(curVal.doubleValue(), newVal.doubleValue());
        appManager.updateFrameIfPaused();
    }

    private void adjustHorOrientation(Number newVal) {
        animationVars.ofSetOrigin = new Point2D(-newVal.doubleValue(), animationVars.ofSetOrigin.getY());
        appManager.updateFrameIfPaused();
    }

    private void adjustVerticalOrientation(Number newVal) {
        animationVars.ofSetOrigin = new Point2D(animationVars.ofSetOrigin.getX(), newVal.doubleValue());
        appManager.updateFrameIfPaused();
    }

    private void adjustThickness(Number newVal) {
        animationVars.armThickness = newVal.doubleValue();
        appManager.updateFrameIfPaused();
    }

    private void zoom(Number curVal, Number newVal) {
        animationVars.scaleFactor = (newVal.doubleValue());
        reScaleVariablesInPathLists(curVal.doubleValue(), newVal.doubleValue());
        appManager.updateFrameIfPaused();
    }

    private void updateArmPartCount(Number newVal) {
        animationVars.setSelectedArmPartIndex((newVal.intValue() - 1));
        appManager.updateFrameIfPaused();
    }

    private static Slider setupHorizontalSlider(GridPane grid, HorSliderInput s, boolean setScale) {
        Slider slider = new Slider(s.minValue, s.maxValue, s.curValue);
        slider.setPrefWidth(SCREEN_SIZE.getWidth() / 2);
        if (setScale) {
            slider.setShowTickLabels(true);
            slider.setShowTickMarks(true);
            slider.setMajorTickUnit(50);
            slider.setMinorTickCount(5);
            slider.setBlockIncrement(10);
        }
        grid.add(slider, s.hPos, s.vPos, s.hSpan, 1);
        return slider;
    }

    private Slider setupVerSlider(GridPane grid) {
        Slider slider = new Slider(-appManager.getScene().getHeight(), appManager.getScene().getHeight(), animationVars.ofSetOrigin.getY());
        slider.setPrefHeight(SCREEN_SIZE.getHeight());
        slider.setOrientation(Orientation.VERTICAL);
        grid.add(slider, 6, 0, 1, vPosControls);
        return slider;
    }

    private void reScaleVariablesInPathLists(double currentScaleFactor, double newScaleFactor) {
        double scaling = newScaleFactor / currentScaleFactor;
        animationVars.specPath = animationVars.specPath.stream().map(n -> n.multiply(scaling)).collect(Collectors.toList());
        animationVars.finalPath = animationVars.finalPath.stream().map(n -> n.multiply(scaling)).collect(Collectors.toList());
    }

    private void rotateVariablesInPathLists(double curAngle, double newAngle) {
        double deltaAngle = newAngle - curAngle;
        Complex angleComplex = new Complex(Math.cos(deltaAngle), -Math.sin(deltaAngle));
        animationVars.specPath = animationVars.specPath.stream()
                .map(n -> n.multiply(angleComplex))
                .collect(Collectors.toList());
        animationVars.finalPath = animationVars.finalPath.stream()
                .map(n -> n.multiply(angleComplex))
                .collect(Collectors.toList());
    }

    public void resetButtonSettings() {
        armsButton.setActive(true);
        specPathButton.setActive(true);
        circlesButton.setActive(false);
        finalPathButton.setActive(false);
        showStatsButton.setActive(false);
    }

    public void setFullScreenButtonMode(boolean fullScreen) {
        this.fullScreenButton.setActive(fullScreen);
    }

    public void zoom(ScrollEvent e) {
        // Adjust the zoom factor as per your requirement
        double zoomFactor = 1;
        if (animationVars.scaleFactor > MIN_SCALE_FACTOR && e.getDeltaY() < 0) {
            zoomFactor = 0.9;
        }
        if (animationVars.scaleFactor < MAX_SCALE_FACTOR && e.getDeltaY() > 0) {
            zoomFactor = 1.1;
        }
        animationVars.scaleFactor = animationVars.scaleFactor * zoomFactor;
        zoomSliderUpdater.accept(animationVars.scaleFactor);
    }

    public DoubleConsumer getHorizontalSliderUpdater() {
        return horizontalSliderUpdater;
    }

    public DoubleConsumer getVerticalSliderUpdater() {
        return verticalSliderUpdater;
    }

    public SwitchButton getFollowButton() {
        return followButton;
    }

    public SwitchButton getSpecPathButton() {
        return specPathButton;
    }

    public SwitchButton getFinalPathButton() {
        return finalPathButton;
    }

    public SwitchButton getDottedLineButton() {
        return dottedLineButton;
    }

    public SwitchButton getMultiButton() {
        return multiButton;
    }

    public SwitchButton getShowStatsButton() {
        return showStatsButton;
    }

    public SwitchButton getCirclesButton() {
        return circlesButton;
    }

    public SwitchButton getArmsButton() {
        return armsButton;
    }
}
