package hzt.view;

import hzt.controller.AppManager;
import hzt.model.EpiCycleSort;
import hzt.model.data.InputValue;
import hzt.model.fourier_transform.EpiCycle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.apache.commons.math3.complex.Complex;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static javafx.animation.Animation.Status.RUNNING;

public class FourierDrawerSingleArm extends FourierDrawer {

    private final ObservableList<EpiCycle> epiCycleList = FXCollections.observableArrayList();

    private final BiFunction<InputValue, Comparator<EpiCycle>, List<EpiCycle>> converterFunction;
    private Complex c;
    private Complex prevC;
    private Complex cRot;
    private Complex cPrevRot;

    public FourierDrawerSingleArm(AppManager appManager,
                                  AnimationVars animationVars,
                                  UserInputController userInputController,
                                  ColorController colorController,
                                  BiFunction<InputValue, Comparator<EpiCycle>, List<EpiCycle>> converterFunction) {
        super(appManager, animationVars, userInputController, colorController);
        Objects.requireNonNull(converterFunction, "The converter function may not be null");
        this.converterFunction = converterFunction;
    }

    void updateVarsAnimation(GraphicsContext graphics) {
        c = Complex.ZERO;
        for (int i = 0; i < epiCycleList.size(); i++) {
            updateVarsByCurCircle(i, graphics);
        }
    }

    @Override
    protected void setRefRadius() {
        animationVars.setRefRadius(epiCycleList);
    }

    @Override
    public void convertInput(InputValue inputValue, EpiCycleSort epiCycleSort) {
        epiCycleList.clear();
        final var epiCycles = converterFunction.apply(inputValue, epiCycleSort.getComparator());
        this.epiCycleList.addAll(epiCycles);
    }

    @Override
    public void sortEpiCycles(Comparator<EpiCycle> comparator) {
        epiCycleList.sort(comparator);
    }

    private void updateVarsByCurCircle(int i, GraphicsContext graphics) {
        prevC = c;
        EpiCycle current = epiCycleList.get(i);
        c = c.add(current.toComplex(animationVars.time).multiply(animationVars.scaleFactor));
        rotateArmByAngle(animationVars.angle);
        setParams(i, current.amp() * animationVars.scaleFactor, graphics);
    }

    private void rotateArmByAngle(double angle) {
        Complex angleComplex = new Complex(cos(angle), -sin(angle));
        cRot = c.multiply(angleComplex);
        cPrevRot = prevC.multiply(angleComplex);
    }

    private void setParams(int i, double radius, GraphicsContext graphics) {
        Color arm = colorController.setRGB(colorController.getArmBegin(), colorController.getArmEnd(), i);
        if (appManager.getTimeline().getStatus() == RUNNING) {
            if (i == epiCycleList.size() - 1) {
                animationVars.finalPath.add(0, cRot);
            }
            if (i == animationVars.selectedArmPartIndex) {
                animationVars.specPath.add(0, cRot);
            }
        }
        if (i <= animationVars.selectedArmPartIndex) {
            drawArmPiece(radius, (radius + AnimationVars.MOD) / (animationVars.refRad + AnimationVars.MOD),
                    colorController.getSpecPathColor(), arm, graphics);
        }
        if (userInputController.getFinalPathButton().isActive()) {
            drawArmPiece(radius, (radius + AnimationVars.MOD) / (animationVars.refRad + AnimationVars.MOD),
                    colorController.getFinalPathColor(), arm, graphics);
        }
    }

    private void drawArmPiece(double radius, double shrinkFactor, Color pathColor, Color arm, GraphicsContext g) {
        double armWidth = animationVars.armThickness * shrinkFactor * sqrt(animationVars.scaleFactor);
        double diameter = radius * 2;
        double hingeRadius = armWidth * .9;
        double circleLineWidth = armWidth / 6;
        if (userInputController.getCirclesButton().isActive()) {
            g.setStroke(colorController.getCircleColor());
            g.setLineWidth(circleLineWidth);
            g.strokeOval(cPrevRot.getReal() - radius, cPrevRot.getImaginary() - radius, diameter, diameter);
        }
        if (userInputController.getArmsButton().isActive()) {
            g.setLineWidth(armWidth);
            g.setStroke(arm);
            g.setFill(pathColor);
            drawArmAndHinge(hingeRadius, g);
        }
    }

    private void drawArmAndHinge(double hingeDiam, GraphicsContext g) {
        double hingeRadius = hingeDiam / 2;
        g.fillOval(cPrevRot.getReal() - hingeRadius, cPrevRot.getImaginary() - hingeRadius, hingeDiam, hingeDiam);
        g.strokeLine(cPrevRot.getReal(), cPrevRot.getImaginary(), cRot.getReal(), cRot.getImaginary());
        g.fillOval(cRot.getReal() - hingeRadius, cRot.getImaginary() - hingeRadius, hingeDiam, hingeDiam);
        g.fillOval(cPrevRot.getReal() - hingeRadius, cPrevRot.getImaginary() - hingeRadius, hingeDiam, hingeDiam);
    }
}
