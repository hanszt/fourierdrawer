package hzt.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

import java.util.List;
import java.util.Random;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.System.nanoTime;
import static javafx.scene.paint.Color.*;

public class ColorController {

    private final Random random = new Random();

    private final ObjectProperty<Color> backgroundColor = new SimpleObjectProperty<>(getRandomColor().darker().darker());
    private final ObjectProperty<Color> circleColor = new SimpleObjectProperty<>(BROWN);
    private final ObjectProperty<Color> finalPathColor = new SimpleObjectProperty<>(getRandomColor());
    private final ObjectProperty<Color> specPathColor = new SimpleObjectProperty<>(getRandomColor().brighter().brighter());
    private final ObjectProperty<Color> armBegin = new SimpleObjectProperty<>(MAROON);
    private final ObjectProperty<Color> armEnd = new SimpleObjectProperty<>(YELLOW);

    private double timer = 0;

    void setChangingColor(UserInputController userInputController) {
        if (userInputController.getMultiButton().isActive()) {
            if (Double.compare(timer, 0) == 0) {
                timer = nanoTime();
            }
            if (nanoTime() - timer > 1e9) {
                finalPathColor.set(getRandomColor());
                specPathColor.set(getRandomColor());
                timer = 0;
            }
        }
    }

    Color setRGB(Color color1, Color color2, int currentPos) {
        final int length = 12;
        final int MAX_RGB = 1;
        double r = (MAX_RGB * (color1.getRed() + ((-cos(PI * currentPos / length) + 1) / 2) * (color2.getRed() - color1.getRed())));
        double g = (MAX_RGB * (color1.getGreen() + ((-cos(PI * currentPos / length) + 1) / 2) * (color2.getGreen() - color1.getGreen())));
        double b = (MAX_RGB * (color1.getBlue() + ((-cos(PI * currentPos / length) + 1) / 2) * (color2.getBlue() - color1.getBlue())));
        return new Color(r, g, b, 1);
    }

    private Color getRandomColor() {
        List<Color> colors = List.of(NAVY, ORANGE, YELLOW, DARKGREEN, LIGHTBLUE, MAROON, RED, MAROON, GREEN, BLUE);
        return colors.get(random.nextInt(colors.size()));
    }

    public void resetColors(UserInputController userInputController) {
        backgroundColor.setValue(getRandomColor().darker().darker());
        armBegin.setValue(MAROON);
        armEnd.set(DARKGREEN);
        circleColor.set(GRAY);
        finalPathColor.set(getRandomColor());
        specPathColor.set(getRandomColor().brighter().brighter());
        userInputController.getMultiButton().setActive(false);
    }

    public Color getBackgroundColor() {
        return backgroundColor.get();
    }

    public ObjectProperty<Color> backgroundColorProperty() {
        return backgroundColor;
    }

    public Color getCircleColor() {
        return circleColor.get();
    }

    public ObjectProperty<Color> circleColorProperty() {
        return circleColor;
    }

    public Color getFinalPathColor() {
        return finalPathColor.get();
    }

    public ObjectProperty<Color> finalPathColorProperty() {
        return finalPathColor;
    }

    public Color getSpecPathColor() {
        return specPathColor.get();
    }

    public ObjectProperty<Color> specPathColorProperty() {
        return specPathColor;
    }

    public Color getArmBegin() {
        return armBegin.get();
    }

    public ObjectProperty<Color> armBeginProperty() {
        return armBegin;
    }

    public Color getArmEnd() {
        return armEnd.get();
    }

    public ObjectProperty<Color> armEndProperty() {
        return armEnd;
    }
}
