package hzt.view;

import hzt.controller.AppManager;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import org.apache.commons.math3.complex.Complex;

public final class StatsController {

    private static final int FONT_SIZE_STATS = 12;
    private static final int STATS_POS_FROM_RIGHT = 300;

    private StatsController() {
    }

    static void showStats(AppManager appManager,
                   UserInputController userInputController,
                   ColorController colorController,
                   GraphicsContext graphics) {
        if(userInputController.getShowStatsButton().isActive()) {
            drawVarNames(appManager, colorController, graphics);
            getStats(appManager, userInputController, colorController, graphics);
        }
    }
    private static void drawVarNames(AppManager appManager, ColorController colorController, GraphicsContext graphics) {
        graphics.setTextBaseline(VPos.TOP);
        graphics.setTextAlign(TextAlignment.LEFT);
        graphics.setFont(Font.font("", FONT_SIZE_STATS));
        graphics.setFill(colorController.getBackgroundColor());
        graphics.fillRect(0, 0, 0, appManager.getScene().getWidth());
        graphics.setFill(colorController.getFinalPathColor());
        final var text = """
                
                
                FourierDrawer stats:
                Input name:
                Number of input points:
                Number of arm parts:
                Position drawer (re, im):
                Frame Count:
                Cycle Count:
                Runtime:
                """.stripIndent();
        graphics.fillText(text, appManager.getScene().getWidth() - STATS_POS_FROM_RIGHT, 0);
    }

    private static void getStats(AppManager appManager,
                          UserInputController userInputController,
                          ColorController colorController,
                          GraphicsContext graphics) {

        FourierDrawer fourierDrawer = appManager.getArmType().drawer();

        final int NUMBER_POS_X = 170;
        final var animationVars = fourierDrawer.animationVars;
        int nrOfIterations = animationVars.selectedArmPartIndex + 1;

        Complex complex =new Complex(0, 0);

        if (!animationVars.finalPath.isEmpty() && !animationVars.specPath.isEmpty()) {
            complex = userInputController.getFinalPathButton().isActive() ?
                    animationVars.finalPath.get(0) :
                    animationVars.specPath.get(0);
        }
        graphics.setFill(colorController.getSpecPathColor());
        final var input = appManager.getInput();
        final var text = String.format("%n%n%s%n%d%n%d%n(%.0f, %.0f)%n%d%n%d%n%.2f s",
                input.name(),
                input.size(),
                nrOfIterations,
                complex.getReal(),
                complex.getImaginary(),
                animationVars.getFrameCount(),
                animationVars.getCycleCount(),
                appManager.getRunTimeSim() / 1e9);

        graphics.fillText(text, appManager.getScene().getWidth() + NUMBER_POS_X - STATS_POS_FROM_RIGHT, FONT_SIZE_STATS);
    }
}
