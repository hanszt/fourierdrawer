package hzt.view;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.Optional;

final class PopUpController {

    private PopUpController() {
    }

    private static Popup createPopup(final String message, boolean warning) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setPrefHeight(10);
        label.setOnMouseReleased(e -> popup.hide());
        final var styleSheetLocation = "/static/css/style.css";
        final var styleSheet = Optional.ofNullable(PopUpController.class.getResource(styleSheetLocation))
                .map(URL::toExternalForm)
                .orElseThrow(() -> new IllegalStateException(styleSheetLocation + " not found"));
        label.getStylesheets().add(styleSheet);
        label.getStyleClass().add(warning ? "popup_warning" : "popup");
        popup.getContent().add(label);
        return popup;
    }

    static void showPopupMessage(final String message, boolean warning, final Stage stage) {
        final Popup popup = createPopup(message, warning);
        popup.setOnShown(e -> setPopupPosition(stage, popup));
        popup.show(stage);
        new Timeline(new KeyFrame(Duration.millis(1400), e -> popup.hide())).play();
    }

    private static void setPopupPosition(Stage stage, Popup popup) {
        popup.setX(stage.getX() + stage.getWidth() / 5 - popup.getWidth() / 2);
        popup.setY(stage.getY() + stage.getHeight() / 2 - popup.getHeight() / 2);
    }
}
