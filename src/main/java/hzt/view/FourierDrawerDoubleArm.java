package hzt.view;

import hzt.controller.AppManager;
import hzt.model.EpiCycleSort;
import hzt.model.data.InputValue;
import hzt.model.fourier_transform.EpiCycle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.util.Pair;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

import static hzt.view.AnimationVars.MOD;
import static java.lang.Math.*;
import static javafx.animation.Animation.Status.RUNNING;

public class FourierDrawerDoubleArm extends FourierDrawer {

    private static final Complex ANGLE_HALF_PI = new Complex(-cos(PI / 2), sin(PI / 2));

    private final ObservableList<EpiCycle> horizontalEpiCycleList = FXCollections.observableArrayList();
    private final ObservableList<EpiCycle> verticalEpiCycleList = FXCollections.observableArrayList();
    private final BiFunction<InputValue, Comparator<EpiCycle>, Pair<List<EpiCycle>, List<EpiCycle>>> converterFunction;
    private Complex c1;
    private Complex c2;
    private Complex prevC1;
    private Complex prevC2;
    private Complex c1R;
    private Complex c2R;
    private Complex prevC1R;
    private Complex prevC2R;

    public FourierDrawerDoubleArm(AppManager manager,
                                  AnimationVars animationVars,
                                  UserInputController userInputController,
                                  ColorController colorController,
                                  BiFunction<InputValue, Comparator<EpiCycle>, Pair<List<EpiCycle>, List<EpiCycle>>> converterFunction) {
        super(manager, animationVars, userInputController, colorController);
        Objects.requireNonNull(converterFunction, "The converter function may not be null");
        this.converterFunction = converterFunction;
    }

    void updateVarsAnimation(GraphicsContext gc) {
        c1 = c2 = new Complex(0, 0);
        for (int i = 0; i < horizontalEpiCycleList.size(); i++) {
            prevC1 = c1;
            prevC2 = c2;
            EpiCycle current1 = horizontalEpiCycleList.get(i);
            c1 = c1.add(current1.toComplex(animationVars.time).multiply(animationVars.scaleFactor));
            EpiCycle current2 = verticalEpiCycleList.get(i);
            c2 = c2.add(current2.toComplex(animationVars.time).multiply(animationVars.scaleFactor).multiply(ANGLE_HALF_PI));
            rotateArmsByAngle(animationVars.angle);
            if (i == animationVars.selectedArmPartIndex) {
                if (appManager.getTimeline().getStatus() == RUNNING) {
                    animationVars.specPath.add(0, new Complex(c1R.getReal(), c2R.getImaginary()));
                }
                if ((userInputController.getArmsButton().isActive() && userInputController.getSpecPathButton().isActive()) ||
                        (userInputController.getShowStatsButton().isActive() && !userInputController.getFinalPathButton().isActive())) {
                    drawLinesToDrawing(animationVars.specPath, c2R.getReal(), c1R.getImaginary(), colorController.getSpecPathColor(), gc);
                }
            }
            double rad1 = current1.amp() * animationVars.scaleFactor;
            double rad2 = current2.amp() * animationVars.scaleFactor;
            if (i <= animationVars.selectedArmPartIndex && !userInputController.getFinalPathButton().isActive()) {
                drawArms(rad1, rad2, (rad1 + MOD) / (animationVars.refRad + MOD), gc);
            }
            if (i == horizontalEpiCycleList.size() - 1 && appManager.getTimeline().getStatus() == RUNNING) {
                animationVars.finalPath.add(0, new Complex(c1R.getReal(), c2R.getImaginary()));
            }
            if (userInputController.getFinalPathButton().isActive()) {
                drawArms(rad1, rad2, (rad1 + MOD) / (animationVars.refRad + MOD), gc);
                if (userInputController.getArmsButton().isActive() && i == horizontalEpiCycleList.size() - 1) {
                    drawLinesToDrawing(animationVars.finalPath, c2R.getReal(), c1R.getImaginary(),
                            colorController.getFinalPathColor(), gc);
                }
            }
        }
    }

    @Override
    protected void setRefRadius() {
        animationVars.setRefRadius(horizontalEpiCycleList);
    }

    @Override
    public void convertInput(InputValue inputValue, EpiCycleSort epiCycleSort) {
        horizontalEpiCycleList.clear();
        verticalEpiCycleList.clear();
        final var pair = converterFunction.apply(inputValue, epiCycleSort.getComparator());
        horizontalEpiCycleList.addAll(pair.getFirst());
        verticalEpiCycleList.addAll(pair.getSecond());
    }

    @Override
    public void sortEpiCycles(Comparator<EpiCycle> comparator) {
        horizontalEpiCycleList.sort(comparator);
        verticalEpiCycleList.sort(comparator);
    }

    private void rotateArmsByAngle(double angle) {
        Complex angleComplex = new Complex(cos(angle), -sin(angle));
        c1R = c1.multiply(angleComplex);
        prevC1R  = prevC1.multiply(angleComplex);
        c2R = c2.multiply(angleComplex);
        prevC2R = prevC2.multiply(angleComplex);
    }

    private void drawArms(double radius1, double radius2, double factor, GraphicsContext gc) {
        double armWidth = animationVars.armThickness * factor * sqrt(animationVars.scaleFactor);
        double hingeDiameter = armWidth * .9;
        double circleLineWidth = armWidth / 4;
        if (userInputController.getCirclesButton().isActive()) {
            gc.setLineWidth(circleLineWidth);
            gc.setStroke(colorController.getArmBegin().darker());
            gc.strokeOval(prevC1R.getReal() - radius1, prevC1R.getImaginary() - radius1, radius1 * 2, radius1 * 2);
            gc.setStroke(colorController.getArmEnd().darker());
            gc.strokeOval(prevC2R.getReal() - radius2, prevC2R.getImaginary() - radius2, radius2 * 2, radius2 * 2);
        }
        if (userInputController.getArmsButton().isActive()) {
            gc.setLineWidth(armWidth);
            gc.setStroke(colorController.getArmBegin());
            gc.strokeLine(prevC1R.getReal(), prevC1R.getImaginary(), c1R.getReal(), c1R.getImaginary());
            gc.setStroke(colorController.getArmEnd());
            gc.strokeLine(prevC2R.getReal(), prevC2R.getImaginary(), c2R.getReal(), c2R.getImaginary());
            drawHinge(hingeDiameter, gc);
        }
    }

    private void drawHinge(double hingeDiameter, GraphicsContext gc) {
        double hingeRadius = hingeDiameter / 2;
        gc.setFill(colorController.getArmBegin().brighter().brighter());
        gc.fillOval(c1R.getReal() - hingeRadius, c1R.getImaginary() - hingeRadius, hingeDiameter, hingeDiameter);
        gc.fillOval(prevC1R.getReal() - hingeRadius, prevC1R.getImaginary() - hingeRadius, hingeDiameter, hingeDiameter);
        gc.setFill(colorController.getArmEnd().brighter().brighter());
        gc.fillOval(c2R.getReal() - hingeRadius, c2R.getImaginary() - hingeRadius, hingeDiameter, hingeDiameter);
        gc.fillOval(prevC2R.getReal() - hingeRadius, prevC2R.getImaginary() - hingeRadius, hingeDiameter, hingeDiameter);
    }

    private void drawLinesToDrawing(List<Complex> list, double reEnd, double imEnd, Color color, GraphicsContext gc) {
        double rePath = list.get(0).getReal();
        double imPath = list.get(0).getImaginary();
        double diameter = animationVars.armThickness / 4;
        gc.setLineWidth(diameter);
        gc.setStroke(colorController.getArmBegin().brighter().brighter());
        gc.strokeLine(rePath, imEnd, rePath, imPath);
        gc.setStroke(colorController.getArmEnd().brighter().brighter());
        gc.strokeLine(rePath, imPath, reEnd, imPath);
        gc.setFill(color);
        double diameter2 = diameter * 2.5;
        gc.fillOval(rePath - diameter, imPath - diameter, diameter2, diameter2);
    }
}
