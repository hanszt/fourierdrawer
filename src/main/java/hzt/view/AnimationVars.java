package hzt.view;

import hzt.model.fourier_transform.EpiCycle;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Point2D;
import org.apache.commons.math3.complex.Complex;

import java.util.ArrayList;
import java.util.List;

import static hzt.utils.AppConstants.INIT_SCENE_SIZE;
import static hzt.model.EpiCycleSort.AMPLITUDE;

public final class AnimationVars {

    //sim constants
    public static final int START_ARM_PART_INDEX = 60;
    static final int MOD = 30;
    static final double PATH_SIZE_FACTOR = 0.95;
    static final double PATH_WIDTH = 2;

    // variables
    int selectedArmPartIndex = START_ARM_PART_INDEX;
    private final IntegerProperty frameCount = new SimpleIntegerProperty();
    private final IntegerProperty cycleCount = new SimpleIntegerProperty();
    private final IntegerProperty maxPathLength = new SimpleIntegerProperty();
    double time;
    double scaleFactor = 1;
    Point2D posOrigin = new Point2D(INIT_SCENE_SIZE.getWidth() / 2., INIT_SCENE_SIZE.getHeight() / 2.);
    Point2D ofSetOrigin = Point2D.ZERO;
    double angle;
    double armThickness = 12;
    double refRad;
    List<Complex> specPath = new ArrayList<>();
    List<Complex> finalPath = new ArrayList<>();

    public void resetAnimation() {
        frameCount.set(0);
        cycleCount.set(0);
        time = angle = 0;
        ofSetOrigin = Point2D.ZERO;
        selectedArmPartIndex = START_ARM_PART_INDEX;
        scaleFactor = 1;
        specPath.clear();
        finalPath.clear();
    }

    public void setRefRadius(List<EpiCycle> list) {
        list.stream()
                .max(AMPLITUDE.getComparator())
                .map(EpiCycle::amp)
                .ifPresent(this::setRefRad);
    }

    public Point2D getPosOrigin() {
        return posOrigin;
    }

    public void setPosOrigin(Point2D posOrigin) {
        this.posOrigin = posOrigin;
    }

    public Point2D getOfSetOrigin() {
        return ofSetOrigin;
    }

    public void setOfSetOrigin(Point2D ofSetOrigin) {
        this.ofSetOrigin = ofSetOrigin;
    }

    public void setSelectedArmPartIndex(int selectedArmPartIndex) {
        this.selectedArmPartIndex = selectedArmPartIndex;
    }

    public void setRefRad(double refRad) {
        this.refRad = refRad;
    }

    public int getCycleCount() {
        return cycleCount.get();
    }

    public void setCycleCount(int cycleCount) {
        this.cycleCount.set(cycleCount);
    }

    public int getFrameCount() {
        return frameCount.get();
    }

    public void setFrameCount(int frameCount) {
        this.frameCount.set(frameCount);
    }

    public int getMaxPathLength() {
        return maxPathLength.get();
    }

    public void setMaxPathLength(int size) {
        maxPathLength.set((int) (PATH_SIZE_FACTOR * size));
    }
}
