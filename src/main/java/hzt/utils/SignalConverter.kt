package hzt.utils

import hzt.model.data.InputGenerator
import hzt.model.data.InputValue
import hzt.model.fourier_transform.EpiCycle
import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.transform.DftNormalization
import org.apache.commons.math3.transform.FastFourierTransformer
import org.apache.commons.math3.transform.TransformType
import org.apache.commons.math3.util.Pair
import org.slf4j.LoggerFactory
import java.util.*
import java.util.function.IntFunction
import kotlin.math.atan2
import kotlin.math.sqrt


const val INPUT_SIGNAL_CONVERTED_IN_SECONDS = "Input signal converted in {} seconds\n"
const val CONVERTING_MESSAGE = "Converting {} (size {}) for single arm...\n"
private val logger = LoggerFactory.getLogger("SignalConverter")

fun initializeSingleArm(inputValue: InputValue, comparator: Comparator<EpiCycle>): List<EpiCycle> {
    val start = System.currentTimeMillis()
    val inputSignal = processInputValueSignal(inputValue) { it }
    val name = inputValue.name
    logger.info(CONVERTING_MESSAGE, name, inputValue.size())
    val epiCycles = toEpiCycleListByDft(inputSignal, comparator)
    logger.info(INPUT_SIGNAL_CONVERTED_IN_SECONDS, (System.currentTimeMillis() - start) / 1e3)
    return epiCycles
}


fun initializeFastFourierApacheCommons(inputValue: InputValue, comparator: Comparator<EpiCycle>): List<EpiCycle> {
    val start = System.currentTimeMillis()
    val inputSignal = processInputValueSignal(inputValue, ::duplicateIfSizeNotPowerOfTwo)
    val name = inputValue.name
    logger.info(CONVERTING_MESSAGE, name, inputValue.size())
    val epiCycles = toEpiCycleListByApacheCommonsFft(inputSignal, comparator)
    logger.info(INPUT_SIGNAL_CONVERTED_IN_SECONDS, (System.currentTimeMillis() - start) / 1e3)
    return epiCycles
}


fun initializeFastFourierOwnImpl(inputValue: InputValue, comparator: Comparator<EpiCycle>): List<EpiCycle> {
    val start = System.currentTimeMillis()
    val inputSignal = processInputValueSignal(inputValue, ::duplicateIfSizeNotPowerOfTwo)
    val name = inputValue.name
    logger.info(CONVERTING_MESSAGE, name, inputValue.size())
    val epiCycles = toEpiCycleListByOwnImplFft(inputSignal, comparator)
    logger.info(INPUT_SIGNAL_CONVERTED_IN_SECONDS, (System.currentTimeMillis() - start) / 1e3)
    return epiCycles
}


fun initializeDoubleArm(
    inputValue: InputValue,
    comparator: Comparator<EpiCycle>,
    toSignalMappers: IntFunction<IntArray>
): Pair<List<EpiCycle>, List<EpiCycle>> {
    val start = System.currentTimeMillis()
    val noiseSignal = toSignalMappers.apply(inputValue.size())
    val inputSignalX = processInputValueSignal(inputValue, noiseSignal, true)
    val inputSignalY = processInputValueSignal(inputValue, noiseSignal, false)
    val name = inputValue.name
    logger.info("Converting {} for double arm...\n", name)
    val epiCyclesX = toEpiCycleListByDft(inputSignalX, comparator)
    val epiCyclesY = toEpiCycleListByDft(inputSignalY, comparator)
    logger.info(INPUT_SIGNAL_CONVERTED_IN_SECONDS, (System.currentTimeMillis() - start) / 1e3)
    return Pair(epiCyclesX, epiCyclesY)
}


fun initializeDoubleArmFft(
    inputValue: InputValue,
    comparator: Comparator<EpiCycle>,
    toSignalMapper: IntFunction<IntArray>
): Pair<List<EpiCycle>, List<EpiCycle>> {
    val start = System.currentTimeMillis()
    val noiseSignal = toSignalMapper.apply(inputValue.size())
    val inputSignalX = processInputValueSignalFft(inputValue, noiseSignal, true)
    val inputSignalY = processInputValueSignalFft(inputValue, noiseSignal, false)
    val name = inputValue.name
    logger.info("Converting {} for double arm...\n", name)
    val epiCyclesX = toEpiCycleListByOwnImplFft(inputSignalX, comparator)
    val epiCyclesY = toEpiCycleListByOwnImplFft(inputSignalY, comparator)
    logger.info(INPUT_SIGNAL_CONVERTED_IN_SECONDS, (System.currentTimeMillis() - start) / 1e3)
    return Pair(epiCyclesX, epiCyclesY)
}


fun noiseSignal(size: Int): IntArray {
    logger.info("Creating noise signal")
    return InputGenerator.getRandomDiscreetValueSignal(10, size)
}


fun duplicateIfSizeNotPowerOfTwo(array: IntArray): IntArray {
    if (isPowerOfTwo(array.size)) {
        return array
    }
    val list = array.toList()
    val newList: LinkedList<Int> = LinkedList(list)
    var index = 0
    var newIndex = 0
    while (!isPowerOfTwo(newList.size)) {
        newList.add(newIndex, list[index])
        newIndex += 2
        index++
    }
    return newList.toIntArray()
}

private fun processInputValueSignal(input: InputValue, inputProcessor: (IntArray) -> IntArray): Array<Complex> {
    val translateX = input.transX.toDouble()
    val translateY = input.transY.toDouble()
    val scalingFactor = input.scaleFactor
    val paddedXArray = inputProcessor(input.xArray())
    val paddedYArray = inputProcessor(input.yArray())
    return Array(paddedXArray.size) { i ->
        val re = (paddedXArray[i] - translateX) * scalingFactor
        val im = (paddedYArray[i] - translateY) * scalingFactor
        Complex(re, im)
    }
}

private fun processInputValueSignal(input: InputValue, noiseArray: IntArray, isXValue: Boolean): Array<Complex> {
    val scalingFactor = input.scaleFactor
    return Array<Complex>(input.size()) { i ->
        val signalOutput =
            (if (isXValue) input.xArray()[i] - input.transX else input.yArray()[i] - input.transY) * scalingFactor
        val noiseOutput = noiseArray[i] * scalingFactor
        Complex(signalOutput, noiseOutput)
    }
}

private fun processInputValueSignalFft(
    input: InputValue,
    noiseArray: IntArray,
    isXValue: Boolean
): Array<Complex> {
    val scalingFactor = input.scaleFactor
    val signal = if (isXValue) input.xArray() else input.yArray()
    val powerOfTwoSignal = duplicateIfSizeNotPowerOfTwo(signal)
    val powerOfTwoNoiseSignal = duplicateIfSizeNotPowerOfTwo(noiseArray)
    return Array(powerOfTwoSignal.size) { i ->
        val value = powerOfTwoSignal[i]
        val signalOutput = (if (isXValue) value - input.transX else value - input.transY) * scalingFactor
        val noiseOutput = powerOfTwoNoiseSignal[i] * scalingFactor
        Complex(signalOutput, noiseOutput)
    }
}


fun toEpiCycleListByApacheCommonsFft(input: Array<Complex>, comparator: Comparator<EpiCycle>): List<EpiCycle> {
    logger.info("Using fast fourier transform from apache commons to convert signal...")
    val transformedArray = FastFourierTransformer(DftNormalization.STANDARD)
        .transform(input, TransformType.FORWARD)
    return convertToSortedEpiCycleList(transformedArray, comparator)
}

private fun toEpiCycleListByOwnImplFft(input: Array<Complex>, comparator: Comparator<EpiCycle>): List<EpiCycle> {
    logger.info("Using fast fourier transform to convert signal...")
    val transformedArray = input.fft()
    return convertToSortedEpiCycleList(transformedArray, comparator)
}


fun toEpiCycleListByDft(input: Array<Complex>, comparator: Comparator<EpiCycle>): List<EpiCycle> {
    logger.info("Using discrete fourier transform to convert signal...")
    val transformedArray = input.dft()
    return convertToSortedEpiCycleList(transformedArray, comparator)
}

private fun convertToSortedEpiCycleList(
    fourierTransform: Array<Complex>,
    comparator: Comparator<EpiCycle>
): List<EpiCycle> = fourierTransform.indices
    .map { freq -> toNormalizedEpiCycle(freq, fourierTransform[freq], fourierTransform.size) }
    .sortedWith(comparator)

private fun toNormalizedEpiCycle(freq: Int, sum: Complex, length: Int): EpiCycle {
    val reNorm = sum.real / length
    val imNorm = sum.imaginary / length
    val amp = sqrt(reNorm * reNorm + imNorm * imNorm)
    val phase = atan2(imNorm, reNorm)
    return EpiCycle(freq.toDouble(), amp, phase)
}
