package hzt.utils

import java.util.*

typealias Maze = Array<Array<Cell>>

fun generateRandomPerfectMaze(dimension: Dimension) =
    generatePerfectMaze(dimension,
        chooseStartPoint = { (width, height) -> GridPoint(getRandCellPos(width), getRandCellPos(height)) },
        chooseAdjCell = ::randomFromList)

//A cell is a white pixel with 4 pixels around it at odd x,y positions
fun generatePerfectMaze(dimension: Dimension,
                        chooseStartPoint: (Dimension) -> GridPoint,
                        chooseAdjCell: (List<Cell>) -> Cell): List<Cell> {
    val (width, height) = dimension
    require(width.isOdd() && height.isOdd()) { "width ($width) and height ($height) must be odd" }
    val total = (width - 1) * (height - 1) / 4
    val (startX, startY) = chooseStartPoint(dimension)
    val grid = createMazeCellGrid(width, height)
    var thisCell = grid[startY][startX]
    val mazeCells = mutableListOf(thisCell)
    var visited = 1
    val cellStack: Deque<Cell> = ArrayDeque()
    while (visited < total) {
        val closedAdjCells = grid.findClosedAdjCells(thisCell)
        if (closedAdjCells.isNotEmpty()) {
            val adjCell = chooseAdjCell(closedAdjCells)
            grid.knockDownWall(thisCell, adjCell)
            cellStack.push(thisCell)
            thisCell = adjCell
            visited++
        } else {
            thisCell = cellStack.pop()
        }
        mazeCells += thisCell
    }
    return mazeCells
}

private fun Int.isOdd(): Boolean = this % 2 == 1

private val random = Random()
private val leftRightUpDown = listOf(
    GridPoint(1, 0),
    GridPoint(-1, 0),
    GridPoint(0, 1),
    GridPoint(0, -1)
)

private fun createMazeCellGrid(width: Int, height: Int): Maze =
    Array(height) { y ->
        Array(width) { x ->
            Cell(x, y, isWall = x % 2 == 0 || y % 2 == 0)
        }
    }

private fun getRandCellPos(sideLength: Int): Int = random.nextInt((sideLength - 1) / 2) * 2 + 1

private fun Maze.findClosedAdjCells(unit: Cell): List<Cell> = leftRightUpDown.asSequence()
    .map { dir -> dir.multiply(2) }
    .map { (x, y) -> GridPoint(unit.x + x, unit.y + y) }
    .filter { it.isInGrid(this) }
    .map { (x, y) -> this[y][x] }
    .filter(::allWallsIntact)
    .toList()

private fun Maze.allWallsIntact(curCell: Cell): Boolean = leftRightUpDown
    .map { (x, y) -> this[curCell.y + y][curCell.x + x] }
    .all(Cell::isWall)

private fun <T> randomFromList(list: List<T>): T = list[random.nextInt(list.size)]

private fun Maze.knockDownWall(thisCell: Cell, adjCell: Cell) {
    val x = (thisCell.x + adjCell.x) / 2
    val y = (thisCell.y + adjCell.y) / 2
    this[y][x].isWall = false
}

@JvmRecord
data class GridPoint(val x: Int, val y: Int) {
    fun <T> isInGrid(grid: Array<Array<T>>): Boolean = x >= 0 && x < grid[0].size && y >= 0 && y < grid.size

    fun multiply(scalar: Int): GridPoint = GridPoint(x * scalar, y * scalar)
}

@JvmRecord
data class Dimension(val width: Int, val height: Int)

class Cell(val x: Int, val y: Int, var isWall: Boolean)
