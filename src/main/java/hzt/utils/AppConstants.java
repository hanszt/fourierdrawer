package hzt.utils;

import javafx.geometry.Dimension2D;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public final class AppConstants {

    public static final Rectangle2D SCREEN_SIZE = Screen.getPrimary().getVisualBounds();

    public static final Dimension2D INIT_SCENE_SIZE = new Dimension2D(
            SCREEN_SIZE.getWidth() * 9/ 12/*800*/, SCREEN_SIZE.getHeight() * 9 / 12 /*600*/);
    public static final double INIT_FRAME_RATE = 30; // f/s

    public static final int FONT_SIZE_ANNOUNCEMENTS = 80;
    public static final int MAX_NUMBER_OF_INSTANCES = 4;
    public static final String TITLE = "Fourier Drawer";

    public static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";

    // http://patorjk.com/software/taag/#p=display&f=Big&t=A%20%20'Image'%20ng
    public static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BRIGHT_RED = "\u001B[91m";
    public static final String TITLE_FX = ANSI_GREEN +
            "  ______               _           _____                                    \n" +
            " |  ____|             (_)         |  __ \\                                  \n" +
            " | |__ ___  _   _ _ __ _  ___ _ __| |  | |_ __ __ ___      _____ _ __       \n" +
            " |  __/ _ \\| | | | '__| |/ _ \\ '__| |  | | '__/ _` \\ \\ /\\ / / _ \\ '__|\n" +
            " | | | (_) | |_| | |  | |  __/ |  | |__| | | | (_| |\\ V  V /  __/ |        \n" +
            " |_|  \\___/ \\__,_|_|  |_|\\___|_|  |_____/|_|  \\__,_| \\_/\\_/ \\___|_|  \n" +
            "                                                                            \n" +
            "                                                                              " +
            ANSI_RESET;

    public static final String CLOSING_MESSAGE = ANSI_BLUE +
            "See you next Time! :)" +
            ANSI_RESET;

    private AppConstants() {
    }
}
