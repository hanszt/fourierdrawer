package hzt.utils;

import hzt.model.data.InputValue;
import hzt.model.image.Image;
import hzt.model.image.Pixel;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.util.Comparator.comparingDouble;
import static java.util.Comparator.comparingInt;

public final class DrawingExtractor {

    private static final int MAXIMUM_SIZE = 8000;

    private DrawingExtractor() {
    }

    public static InputValue extractNewInputValue(Image image) {
        List<Pixel> reducedList = toReducedSortedPixelList(image);
        final var reducedListSize = reducedList.size();
        int[] xArray = new int[reducedListSize];
        int[] yArray = new int[reducedListSize];
        for (int i = 0; i < reducedListSize; i++) {
            final var pixel = reducedList.get(i);
            xArray[i] = pixel.x();
            yArray[i] = pixel.y();
        }
        String name = format("Loaded %s size: %d", image.name(), reducedListSize);
        return new InputValue(name, xArray, yArray, image.getOriginX(), image.getOriginY(), 1);
    }

    @NotNull
    static List<Pixel> toReducedSortedPixelList(Image image) {
        List<Pixel> linePixelList = getLinePixelList(image);
        int size = linePixelList.size();
        final int reductionFactor = (size >= MAXIMUM_SIZE ? (linePixelList.size() / MAXIMUM_SIZE) : 0) + 1;
        return IntStream.range(0, size)
                .filter(index -> index % reductionFactor == 0)
                .mapToObj(linePixelList::get)
                .sorted(comparingDouble(image::distanceFromOrigin))
                .sorted(comparingInt(image::angleRangeFromOrigin).reversed())
                .toList();
    }

    private static List<Pixel> getLinePixelList(Image image) {
        return Arrays.stream(image.pixels())
                .flatMap(Arrays::stream)
                .filter(Pixel::isLine)
                .toList();
    }
}
