package hzt.utils.io;

import hzt.model.image.Image;
import hzt.model.image.Pixel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.Color.GRAY;
import static javax.imageio.ImageIO.read;

public final class ImageLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageLoader.class);
    public static final String INPUT_DIRECTORY = "input";

    private ImageLoader() {
    }

    public static Image loadImage(File file) {
        String name = file.getName();
        try {
            LOGGER.info("Loading image...");
            BufferedImage bufferedImage = read(file);
            int width = bufferedImage.getWidth();
            int height = bufferedImage.getHeight();
            Pixel[][] pixelDataImage = new Pixel[width][height];
            for (int y = 0; y < bufferedImage.getHeight(); y++) {
                for (int x = 0; x < bufferedImage.getWidth(); x++) {
                    int rgb = bufferedImage.getRGB(x, y);
                    boolean isDrawLine = rgb < GRAY.getRGB();
                    pixelDataImage[x][y] = new Pixel(x, y, isDrawLine);
                }
            }
            LOGGER.info("Data of {} has been loaded.\n", name);
            return new Image(name, pixelDataImage);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read image with name: %s".formatted(file), e);
        } catch (OutOfMemoryError e) {
            throw new IllegalStateException("It takes too much memory to load %s.%n".formatted(name), e);
        }
    }
}
