package hzt.utils.io;

import hzt.model.image.Image;
import hzt.model.image.Pixel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static hzt.utils.AppConstants.ANSI_BRIGHT_RED;
import static hzt.utils.AppConstants.ANSI_RESET;
import static javax.imageio.ImageIO.write;

public final class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);
    private static final String OUTPUT_DIRECTORY = "output";
    public static final String IMAGE_FORMAT = "png";

    private FileUtils() {
    }

    public static String loadTextFromResourceFile(String name) {
        return Optional.ofNullable(FileUtils.class.getResource(name))
                .map(URL::getFile)
                .map(File::new)
                .map(File::toPath)
                .map(FileUtils::readText)
                .orElseThrow(() -> new IllegalStateException("Could not load text from " + name));
    }

    private static String readText(Path path) {
        try {
            return Files.readString(path);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String createOutputImagesOfMazeList(List<Image> imageList) {
        imageList.forEach(FileUtils::createOutputImage);
        String outputImageMessage = imageList.isEmpty() ?
                "No mazes created yet.." :
                (imageList.size() + " images created. Look in the output folder...");
        LOGGER.info(outputImageMessage);
        return outputImageMessage;
    }

    private static void createOutputImage(Image image) {
        LOGGER.info("OutputImage is being created...");
        try {
            final var name = image.name();
            constructImage(image, image.id() + name);
            LOGGER.info("OutputImage of {} created", name);
        } catch (OutOfMemoryError e) {
            LOGGER.error("It takes too much memory to create an image this size...", e);
        }
    }

    private static void constructImage(Image image, String outputName) {
        try {
            BufferedImage bufferedImage = new BufferedImage(image.width(), image.height(), BufferedImage.TYPE_INT_RGB);
            Pixel[][] pixels = image.pixels();
            for (int y = 0; y < pixels[0].length; y++) {
                for (int x = 0; x < pixels.length; x++) {
                    bufferedImage.setRGB(x, y, pixels[x][y].isLine() ? 0 : (255 * 65536 + 255 * 256 + 255));
                }
            }
            File outputFile = new File(OUTPUT_DIRECTORY, outputName + "." + IMAGE_FORMAT);
            write(bufferedImage, IMAGE_FORMAT, outputFile);
        } catch (IOException e) {
            LOGGER.error("Could not construct image", e);
        }
    }

    public static String deleteOutputDirImages() {
        return deleteFiles(new File(OUTPUT_DIRECTORY));
    }

    public static String deleteFiles(File value) {
        final var files = Optional.of(value)
                .filter(File::isDirectory)
                .map(File::listFiles)
                .orElse(null);
        return files != null ? deleteFiles(files, file -> file.getName().endsWith(IMAGE_FORMAT)) : "No images deleted";
    }

    private static String deleteFiles(File[] files, Predicate<File> filePredicate) {
        int counter = 0;
        for (File file : files) {
            if (filePredicate.test(file)) {
                delete(file);
                counter++;
            }
        }
        String deletionMessage = counter + " files deleted.";
        LOGGER.info("{}{}{}", ANSI_BRIGHT_RED, deletionMessage, ANSI_RESET);
        return deletionMessage;
    }

    private static void delete(File file) {
        try {
            Files.delete(file.toPath());
            LOGGER.info("{} is deleted", file);
        } catch (IOException e) {
            LOGGER.error("Failed to delete {}", file, e);
        }
    }
}
