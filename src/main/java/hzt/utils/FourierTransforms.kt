package hzt.utils

import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.complex.Complex.*
import java.util.Arrays
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

/**
 * The Fourier transform breaks down an input signal into doubleArmController collection of sine waves
 *
 * This naive implementation of the Fourier transform has O(n^2) time complexity.
 *
 * It can be reduced to O(n * log (n)) by using the recursive fast fourier transform implementation (See below)
 *
 * @receiver an array of complex nrs representing x and y coordinates
 * @return the fourier transform
 */
fun Array<Complex>.dft() = Array(size) { freq ->
    indices.fold(ZERO) { sum, n ->
        val phi = -2 * PI * freq * n / size
        sum + this[n] * Complex(cos(phi), sin(phi))
    }
}

/**
 * A fastFourier transform implementation
 * This algorithm has a time complexity of O(n * log (n))
 * It is assumed the length of the input array is a power of two
 *
 * @receiver the complex nr array to apply the fast fourier transform to
 * @return the fourier transform of the input
 */
fun Array<Complex>.fft(): Array<Complex> {
    if (size == 1) return arrayOf(this[0])
    require(isPowerOfTwo(size)) { "The length: $size is not a power of 2" }

    val halfSize = size / 2

    val array = Array(halfSize) { this[2 * it] } // reuse the array for even and odd
    val evenFft = array.fft()
    val oddFft = array.also { for (i in it.indices) it[i] = this[2 * i + 1] }.fft()

    @Suppress("UNCHECKED_CAST")
    return arrayOfNulls<Complex>(size).apply {
        for (k in 0..<halfSize) {
            val phi = -2 * PI * k / size
            val even = evenFft[k]
            val odd = oddFft[k] * Complex(cos(phi), sin(phi))
            this[k] = even + odd
            this[k + halfSize] = even - odd
        }
    } as Array<Complex>
}

/**
 * compute the inverse FFT of the input, assuming its length n is a power of 2
 *
 * @receiver the fourierTransform to inverse
 * @return the inverse of the fourier transform
 */
fun Array<Complex>.inverseFft(): Array<Complex> = Array(size) { this[it].conjugate() }
    .fft()
    .replaceAll { conjugate() / size.toDouble() }

private fun <T> Array<T>.replaceAll(replace: T.() -> T): Array<T> = apply { Arrays.setAll(this) { this[it].replace() } }

/**
 * compute the circular convolution of signal1 and signal2
 *
 * @return the circular convolution
 */
fun convolveCircular(signal1: Array<Complex>, signal2: Array<Complex>): Array<Complex> {
    require(signal1.size == signal2.size) { "Dimensions don't agree: ${signal1.size} != ${signal2.size}" }
    val fft1 = signal1.fft()
    val fft2 = signal2.fft()
    return Array(signal1.size) { fft1[it] * fft2[it] }.inverseFft()
}

// compute the linear convolution of x and y
fun convolve(input1: Array<Complex>, input2: Array<Complex>): Array<Complex> =
    convolveCircular(input1.padToPowerOfTwoLength(), input2.padToPowerOfTwoLength())

private fun Array<Complex>.padToPowerOfTwoLength() = this + Array(size.nextPowerOfTwo() - size) { ZERO }
