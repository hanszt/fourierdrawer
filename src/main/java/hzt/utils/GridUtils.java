package hzt.utils;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.function.IntFunction;

public final class GridUtils {

    private GridUtils() {
    }

    @NotNull
    public static <T> T[][] copy(T[][] grid, IntFunction<T[][]> generator) {
        return Arrays.stream(grid)
                .map(row -> Arrays.copyOf(row, row.length))
                .toArray(generator);
    }
}
