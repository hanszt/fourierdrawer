package hzt.utils

import org.apache.commons.math3.complex.Complex
import kotlin.math.max


/**
 * @see [The Smallest power of 2 greater than or equal to n](https://www.geeksforgeeks.org/smallest-power-of-2-greater-than-or-equal-to-n/)
 *
 * @receiver the input nr
 * @return Smallest power of 2 greater than or equal to n
 */
fun Int.nextPowerOfTwo(): Int {
    if (isPowerOfTwo(this)) {
        return this
    }
    var m = max(this, 0)
    var count = 0
    while (m != 0) {
        m = m shr 1
        count++
    }
    val result = 1 shl count
    require(result >= 0) { "No next power of two found for $this" }
    return result
}

fun isPowerOfTwo(n: Int) = (n > 0) && ((n and (n - 1)) == 0)

operator fun Complex.plus(other: Complex): Complex = add(other)
operator fun Complex.minus(other: Complex): Complex = subtract(other)
operator fun Complex.times(other: Complex): Complex = multiply(other)
operator fun Complex.div(other: Double): Complex = divide(other)
