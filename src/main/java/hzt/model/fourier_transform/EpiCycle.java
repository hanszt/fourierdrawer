package hzt.model.fourier_transform;

import org.apache.commons.math3.complex.Complex;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public record EpiCycle(double freq, double amp, double phase) {

    public Complex toComplex(double time) {
        return new Complex(amp * cos(freq * time + phase), amp * sin(freq * time + phase));
    }

}
