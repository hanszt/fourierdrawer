package hzt.model;

import hzt.model.fourier_transform.EpiCycle;

import java.util.Comparator;

public enum EpiCycleSort {

    AMPLITUDE("amplitude (ascending)", Comparator.comparing(EpiCycle::amp)),
    FREQUENCY("frequency (ascending)", Comparator.comparing(EpiCycle::freq)),
    PHASE("phase (ascending)", Comparator.comparing(EpiCycle::freq)),
    REVERSED_AMPLITUDE("amplitude (descending)", Comparator.comparing(EpiCycle::amp).reversed()),
    REVERSED_FREQUENCY("frequency (descending)", Comparator.comparing(EpiCycle::freq).reversed()),
    REVERSED_PHASE("phase (descending)", Comparator.comparing(EpiCycle::phase).reversed());

    private final String description;
    private final Comparator<EpiCycle> comparator;

    EpiCycleSort(String description, Comparator<EpiCycle> comparator) {
        this.description = description;
        this.comparator = comparator;
    }

    public String getDescription() {
        return description;
    }

    public Comparator<EpiCycle> getComparator() {
        return comparator;
    }
}
