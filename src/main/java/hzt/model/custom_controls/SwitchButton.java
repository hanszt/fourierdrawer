package hzt.model.custom_controls;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;

public final class SwitchButton extends Button {

    private final String activeLabel;
    private final String inActiveLabel;
    private final MultipleEventsHandler multipleEventsHandler;

    private final BooleanProperty active = new SimpleBooleanProperty();
    private final BooleanProperty switchModeEnabled = new SimpleBooleanProperty();
    private final StringProperty activeStyle = new SimpleStringProperty();
    private final StringProperty inActiveStyle = new SimpleStringProperty();

    public SwitchButton() {
        this(false);
    }

    private SwitchButton(boolean active) {
        this(active, "active", "off");
    }

    public SwitchButton(boolean active, String enabledText, String disabledText) {
        this(active, enabledText, disabledText, null);
    }

    private SwitchButton(boolean active, String enabledText, String disabledText, Node node) {
        super(active ? enabledText : disabledText, node);
        this.active.setValue(active);
        this.activeLabel = enabledText;
        this.inActiveLabel = disabledText;
        this.multipleEventsHandler = new MultipleEventsHandler();
        this.multipleEventsHandler.addEventHandler(e -> updateButtonIfSwitchModeEnabled());
        this.setOnAction(multipleEventsHandler);
        this.switchModeEnabled.setValue(true);
    }

    private void updateButtonIfSwitchModeEnabled() {
        if (switchModeEnabled.get()) {
            this.active.setValue(!this.active.get());
            this.setButtonLabel();
            this.setStyleByStatus();
        }
    }

    private void setButtonLabel() {
        super.setText(active.get() ? activeLabel : inActiveLabel);
    }

    private void setStyleByStatus() {
        super.setStyle(active.get() ? activeStyle.get() : inActiveStyle.get());
    }

    public boolean isActive() {
        return active.get();
    }

    public BooleanProperty activeProperty() {
        return active;
    }

    public void setActive(boolean active) {
        this.active.setValue(active);
        setToSwitchModeLabelAndStyle();
    }

    private void setToSwitchModeLabelAndStyle() {
        setButtonLabel();
        setStyleByStatus();
    }

    public void setSwitchModeEnabled(boolean switchModeEnabled) {
        this.switchModeEnabled.setValue(switchModeEnabled);
        setToSwitchModeLabelAndStyle();
    }

    public void setButtonFocused(boolean focused) {
        this.setFocused(focused);
    }

    public void setActiveStyle(String activeStyle) {
        this.activeStyle.setValue(activeStyle);
    }

    public MultipleEventsHandler getMultipleEventsHandler() {
        return multipleEventsHandler;
    }
}
