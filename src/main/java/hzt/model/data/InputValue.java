package hzt.model.data;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public record InputValue(String name, int[] xArray, int[] yArray, int transX, int transY, double scaleFactor)
        implements Comparable<InputValue> {
    public InputValue(
            String name,
            int[] xArray,
            int[] yArray,
            int transX,
            int transY,
            double scaleFactor) {
        this.name = name;
        this.xArray = Arrays.copyOf(xArray, xArray.length);
        this.yArray = Arrays.copyOf(yArray, yArray.length);
        this.transX = transX;
        this.transY = transY;
        this.scaleFactor = scaleFactor;
    }

    public InputValue(
            String name,
            int[][] coordinates,
            int transX,
            int transY,
            double scaleFactor) {
        this(name, coordinates[0], coordinates[1], transX, transY, scaleFactor);
    }

    public int size() {
        if (xArray.length != yArray.length) {
            throw new IllegalStateException("x and y arrays differ in length");
        }
        return xArray.length;
    }

    @Override
    public int compareTo(@NotNull InputValue o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InputValue that = (InputValue) o;
        return transX == that.transX && transY == that.transY && Double.compare(that.scaleFactor, scaleFactor) == 0 &&
                Objects.equals(name, that.name) && Arrays.equals(xArray, that.xArray) && Arrays.equals(yArray, that.yArray);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, transX, transY, scaleFactor);
        result = 31 * result + Arrays.hashCode(xArray);
        result = 31 * result + Arrays.hashCode(yArray);
        return result;
    }

    @Override
    public String toString() {
        return "InputValue{" +
                "name='" + name + '\'' +
                ", xArray=" + Arrays.toString(xArray) +
                ", yArray=" + Arrays.toString(yArray) +
                ", transX=" + transX +
                ", transY=" + transY +
                ", scaleFactor=" + scaleFactor +
                '}';
    }

    @Override
    public int[] xArray() {
        return Arrays.copyOf(xArray, xArray.length);
    }

    @Override
    public int[] yArray() {
        return Arrays.copyOf(yArray, yArray.length);
    }

}
