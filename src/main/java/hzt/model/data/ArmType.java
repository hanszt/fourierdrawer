package hzt.model.data;

import hzt.view.FourierDrawer;
import org.jetbrains.annotations.NotNull;

public record ArmType(String name, FourierDrawer drawer) implements Comparable<ArmType> {

    @Override
    public int compareTo(@NotNull ArmType o) {
        return this.name.compareTo(o.name);
    }
}
