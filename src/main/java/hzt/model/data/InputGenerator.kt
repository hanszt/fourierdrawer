package hzt.model.data

import hzt.model.image.Image
import hzt.utils.*
import hzt.utils.io.FileUtils
import hzt.utils.io.ImageLoader
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import java.util.function.IntFunction
import java.util.stream.IntStream
import kotlin.math.pow
import kotlin.math.sin

object InputGenerator {
    private val LOGGER = LoggerFactory.getLogger(InputGenerator::class.java)
    private val RANDOM = Random()
    private const val MAX_NUMBER = 10
    private const val MIN_NUMBER = 2
    private val INPUTS: MutableSet<InputValue> = Collections.newSetFromMap(WeakHashMap())
    private val IMAGES: MutableSet<Image> = Collections.newSetFromMap(WeakHashMap())
    private const val NR_OF_INPUT_ELEMENTS = 2048
    private const val RANGE = 600
    private fun readCoordinates(resourceName: String): Array<IntArray> {
        fun lineToIntArray(line: String) =
            line.split(',')
                .filter(String::isNotEmpty)
                .map { s -> s.trim().toInt() }
                .toIntArray()
        return FileUtils.loadTextFromResourceFile("/input/$resourceName")
            .lines()
            .map(::lineToIntArray)
            .toTypedArray()
    }

    private val sawToothSignal: IntArray
        get() {
            val ref = 800
            val array = IntArray(NR_OF_INPUT_ELEMENTS)
            for (i in 0 until NR_OF_INPUT_ELEMENTS) {
                array[i] = ref * (i - NR_OF_INPUT_ELEMENTS / 2) / NR_OF_INPUT_ELEMENTS
            }
            return array
        }

    private fun getAlternatingSignal(n: Int): IntArray {
        val array = IntArray(NR_OF_INPUT_ELEMENTS)
        array[0] = RANGE / 2
        var counter = 0
        for (i in 1 until NR_OF_INPUT_ELEMENTS - 1) {
            if (i % (NR_OF_INPUT_ELEMENTS / n) == 0 || i == 1) {
                array[i] = (-1.0).pow(counter.toDouble()).toInt() * (RANGE - RANGE / 2)
                counter++
            } else {
                array[i] = array[i - 1]
            }
        }
        array[NR_OF_INPUT_ELEMENTS - 1] = RANGE / 2
        return array
    }

    @JvmStatic
    fun getRandomDiscreetValueSignal(n: Int, nrOfElements: Int): IntArray {
        val array = IntArray(nrOfElements)
        array[0] = RANGE / 2
        try {
            for (i in 1 until nrOfElements - 1) {
                array[i] =
                    if (i % (nrOfElements / n) == 0 || i == 1) RANDOM.nextInt(RANGE - RANGE / 2) else array[i - 1]
            }
            array[nrOfElements - 1] = RANGE / 2
        } catch (e: ArithmeticException) {
            LOGGER.error("", e)
        }
        return array
    }

    private fun getSineSignal(
        n: Int,
        phase: Double,
        factorGenerator: (Int, Int) -> Double = { _, _ ->  1.0 }
    ): IntArray {
        val array = IntArray(NR_OF_INPUT_ELEMENTS)
        for (i in 0 until NR_OF_INPUT_ELEMENTS) {
            val factor = factorGenerator(NR_OF_INPUT_ELEMENTS, i)
            array[i] = -(factor * (RANGE / 2.0) * sin(n * Math.PI * i / NR_OF_INPUT_ELEMENTS + phase)).toInt()
        }
        return array
    }

    @JvmStatic
    fun duplicateEveryInputValue(input: InputValue, factor: Int): InputValue {
        val newSize = input.size() * factor
        val newXValues = IntArray(newSize)
        val newYValues = IntArray(newSize)
        for (i in 0 until newSize) {
            newXValues[i] = input.xArray()[i / factor]
            newYValues[i] = input.yArray()[i / factor]
        }
        return InputValue(input.name, newXValues, newYValues, input.transX, input.transY, input.scaleFactor)
    }

    private fun increaseInputByInterpolation(input: InputValue, factor: Int): InputValue {
        val newSize = input.size() * factor
        val newXValues = IntArray(newSize)
        val newYValues = IntArray(newSize)
        for (i in 0 until input.size()) {
            newXValues[i * factor] = input.xArray()[i]
            var x = newXValues[i * factor]
            newYValues[i * factor] = input.yArray()[i]
            var y = newYValues[i * factor]
            val nextX = getNext(input.xArray(), i)
            val nextY = getNext(input.yArray(), i)
            for (j in 1 until factor) {
                newXValues[i * factor + j] = (x + nextX) / 2
                x = newXValues[i * factor + j]
                newYValues[i * factor + j] = (y + nextY) / 2
                y = newYValues[i * factor + j]
            }
        }
        return InputValue(input.name, newXValues, newYValues, input.transX, input.transY, input.scaleFactor)
    }

    private fun getNext(array: IntArray, i: Int): Int {
        return if (i != array.size - 1) array[i + 1] else array[i]
    }

    private fun getOrderedSquareWave(n: Int): InputValue {
        return InputValue(
            String.format("Ordered square wave (%02d waves)", n),
            sawToothSignal,
            getAlternatingSignal(n),
            -100,
            0,
            1.0
        )
    }

    private fun getRandomSquareWave(n: Int): InputValue {
        return InputValue(
            String.format("Random square wave %02d waves)", n),
            sawToothSignal,
            getRandomDiscreetValueSignal(n, NR_OF_INPUT_ELEMENTS), -100, 0, 1.0
        )
    }

    private fun getSineWave(n: Int, phase: Double): InputValue {
        return InputValue(
            String.format("Sine wave (%02d waves, %.2f phase shift)", n, phase),
            sawToothSignal,
            getSineSignal(n, phase),
            0, 0, 1.0
        )
    }

    private fun getSpiral(m: Int, n: Int): InputValue {
        return InputValue(
            String.format("SpiralShape (%02d, %02d)", m, n),
            getSineSignal(m, 0.0) { nrOfElements, i -> i.toDouble() / (nrOfElements - 1) },
            getSineSignal(n, Math.PI / 2) { nrOfElements, i -> i.toDouble() / (nrOfElements - 1) },
            0, 0, 1.0
        )
    }

    private fun getLissajous(m: Int, n: Int): InputValue {
        return InputValue(
            String.format("Lissajous (%02d, %02d)", m, n),
            getSineSignal(m, 0.0),
            getSineSignal(n, Math.PI / 2),
            0, 0, 1.0
        )
    }

    private fun getRandomMazeInputValue(width: Int, height: Int): InputValue {
        val scaleFactor = 700
        LOGGER.info("Generating maze ({} by {})...\n", width, height)
        val list = generateRandomPerfectMaze(Dimension(width, height))
        LOGGER.info("Maze generated")
        val factor = scaleFactor / height
        return InputValue(
            String.format("Random Maze (%03d by %03d)", width, height),
            toIntArray(list, Cell::x),
            toIntArray(list, Cell::y),
            width / 2 - 4,
            height / 2,
            factor.toDouble()
        )
    }

    private fun getMazeInputValue(width: Int, height: Int): InputValue {
        val scaleFactor = 700
        LOGGER.info("Generating maze ({} by {})...\n", width, height)
        val list = generatePerfectMaze(Dimension(width, height), { _ -> GridPoint(1, 1) }) { it[it.size - 1] }
        LOGGER.info("Maze generated")
        val factor = scaleFactor / height
        return InputValue(
            String.format("Maze (%03d by %03d)", width, height), toIntArray(list, Cell::x), toIntArray(list, Cell::y),
            width / 2 - 4, height / 2, factor.toDouble()
        )
    }

    private fun <T> toIntArray(collection: Collection<T>, toIntFunction: (T) -> Int): IntArray {
        return collection
            .map(toIntFunction)
            .toIntArray()
    }

    @JvmStatic
    val randomInputValue: InputValue
        get() {
            val selector = RANDOM.nextInt(INPUTS.size)
            return INPUTS
                .drop(selector)
                .first()
        }

    @JvmStatic
    fun storeUserInputValue(userInput: InputValue) {
        INPUTS.add(userInput)
    }

    @JvmStatic
    fun fillInputList() {
        val imagesAndInputValues = extractImageListAndInputValues()
        IMAGES.addAll(imagesAndInputValues.first)
        INPUTS.addAll(
            listOf(
                duplicateEveryInputValue(
                    InputValue("Mini figure", readCoordinates("minifig.txt"), 200, 200, 1.8), 2
                ),
                increaseInputByInterpolation(
                    InputValue("G key", readCoordinates("gKey.txt"), 100, 160, 2.3), 4
                ),
                increaseInputByInterpolation(
                    InputValue("Cat", readCoordinates("cat.txt"), 100, 120, 5.0), 4
                ),
                InputValue("Square", readCoordinates("square.txt"), 0, 0, 5.0),
                increaseInputByInterpolation(
                    InputValue("Faces", readCoordinates("faces.txt"), 200, 380, 0.9), 3
                ),
                increaseInputByInterpolation(
                    InputValue("Deer", readCoordinates("deer.txt"), 500, 520, 0.8), 2
                ),
                increaseInputByInterpolation(
                    InputValue("Elephant", readCoordinates("elephant.txt"), 250, 350, 1.1), 3
                ),
                increaseInputByInterpolation(
                    InputValue("Nederland", readCoordinates("nederland.txt"), 370, 390, 0.85), 2
                ),
                increaseInputByInterpolation(
                    InputValue("New York skyline", readCoordinates("newyork.txt"), 380, 300, 1.0), 3
                ),
                increaseInputByInterpolation(
                    InputValue("Dolfin", readCoordinates("dolphin.txt"), 379, 380, 1.5), 2
                ),
                increaseInputByInterpolation(
                    InputValue("Gears", readCoordinates("gear.txt"), 500, 380, 1.0), 3
                ),
                increaseInputByInterpolation(
                    InputValue("Drawing hand", readCoordinates("drawinghand.txt"), 300, 250, 1.2), 3
                ),
                duplicateEveryInputValue(
                    InputValue("Drawing man", readCoordinates("drawingman.txt"), 380, 320, 1.0), 2
                ),
                duplicateEveryInputValue(
                    InputValue("Step square", intArrayOf(-50, 50, 50, -50), intArrayOf(-50, -50, 50, 50), 0, 0, 5.0),
                    256
                ),
                getSpiral(2, 2),
                getSpiral(20, 20),
                getSpiral(20, 5),
                getSpiral(50, 10),
                getSpiral(3, 7),
                getLissajous(1, 1),
                getLissajous(2, 2),
                getLissajous(2, 3),
                getLissajous(2, 4),
                getLissajous(10, 12),
                getLissajous(4, 5),
                getLissajous(8, 9),
                getLissajous(8, 10),
                getLissajous(9, 10),
                getLissajous(9, 11),
                getLissajous(16, 18),
                getLissajous(64, 66),
                getLissajous(6, 8),
                getLissajous(2, 24),
                getSineWave(2, 0.0),
                getSineWave(4, Math.PI / 2),
                getRandomMazeInputValue(91, 121),
                getRandomMazeInputValue(81, 101),
                getRandomMazeInputValue(51, 61),
                getMazeInputValue(51, 51),
                getMazeInputValue(91, 121)
            )
        )
        INPUTS.addAll(buildWaveSignalList(::getRandomSquareWave))
        INPUTS.addAll(buildWaveSignalList(::getOrderedSquareWave))
        INPUTS.addAll(imagesAndInputValues.second)
    }

    private fun buildWaveSignalList(inputValueGenerator: IntFunction<InputValue>): List<InputValue> {
        return IntStream.rangeClosed(MIN_NUMBER, MAX_NUMBER)
            .mapToObj(inputValueGenerator)
            .toList()
    }

    private fun extractImageListAndInputValues(): Pair<List<Image>, List<InputValue>> {
        val file = File(ImageLoader.INPUT_DIRECTORY)
        val files = file.listFiles()
        if (files == null) {
            LOGGER.error("directory '{}' does not exist in root folder", ImageLoader.INPUT_DIRECTORY)
            return Pair(emptyList(), emptyList())
        }
        return try {
            val images = mutableListOf<Image>()
            val inputValues = mutableListOf<InputValue>()
            files.asSequence()
                .map(ImageLoader::loadImage)
                .forEach{ image ->
                    images.add(image)
                    inputValues.add(DrawingExtractor.extractNewInputValue(image))
                }
            images to inputValues
        } catch (e: IllegalStateException) {
            LOGGER.error("input directory not found...", e)
            Pair(emptyList(), emptyList())
        }
    }

    @JvmStatic
    val inputAsSortedList: List<InputValue>
        get() = INPUTS.sortedBy(InputValue::name)
    @JvmStatic
    val images: List<Image>
        get() = IMAGES.toList()

}
