package hzt.model.image;

public record Pixel(int x, int y, boolean isLine) {
}
