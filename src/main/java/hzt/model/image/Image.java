package hzt.model.image;

import hzt.utils.GridUtils;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.Math.atan2;
import static java.lang.Math.sqrt;

public final class Image {

    private static int nrOfImages = 0;
    private final int id;
    private final String name;
    private final Pixel[][] pixels;

    public Image(int id, String name, Pixel[][] pixels) {
        this.id = id;
        this.name = name;
        this.pixels = GridUtils.copy(pixels, Pixel[][]::new);
    }

    public Image(String name, Pixel[][] pixels) {
        this(++nrOfImages, name, pixels);
    }

    public int width() {
        return pixels.length;
    }

    public int height() {
        return pixels[0].length;
    }

    public int id() {
        return id;
    }

    public String name() {
        return name;
    }

    public Pixel[][] pixels() {
        return GridUtils.copy(pixels, Pixel[][]::new);
    }

    public int angleRangeFromOrigin(Pixel pixel) {
        int precision = 4;
        return (int) (precision * atan2(pixel.y() - getOriginY(), pixel.x() - getOriginX()));
    }

    public double distanceFromOrigin(Pixel pixel) {
        var xLength = pixel.x() - getOriginX();
        var yLength = pixel.y() - getOriginY();
        return sqrt(xLength * xLength + yLength * yLength);
    }

    public int getOriginX() {
        return width() / 2;
    }

    public int getOriginY() {
        return height() / 2;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (Image) obj;
        return this.id == that.id &&
                Objects.equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        final var pixelsAsString = Arrays.stream(pixels)
                .map(Arrays::toString)
                .collect(Collectors.joining("\n"));
        return "Image[" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "pixels=" + pixelsAsString + ']';
    }

}
