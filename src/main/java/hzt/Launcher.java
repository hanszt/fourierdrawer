package hzt;

import hzt.controller.AppManager;
import hzt.model.data.InputGenerator;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author Hans Zuidervaart
 */
public class Launcher extends Application {

    public static void main(String... args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        InputGenerator.fillInputList();
        AppManager manager = new AppManager();
        manager.setupStage(stage);
    }

}
