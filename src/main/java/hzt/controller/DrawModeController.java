package hzt.controller;

import hzt.model.data.InputValue;
import hzt.view.AnimationVars;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;
import java.util.List;

import static hzt.model.data.InputGenerator.duplicateEveryInputValue;
import static hzt.model.data.InputGenerator.storeUserInputValue;
import static java.lang.Math.min;

public class DrawModeController {

    private static final int FONT_SIZE_DRAW_ANNOUNCEMENTS = 40;
    private final IntegerProperty userDrawCount = new SimpleIntegerProperty();

    private final StackPane drawRoot;
    private final ObjectProperty<Canvas> drawCanvas = new SimpleObjectProperty<>();

    private final AppManager appManager;

    DrawModeController(AppManager appManager) {
        this.appManager = appManager;
        drawRoot = new StackPane();
        drawRoot.setAlignment(Pos.BOTTOM_LEFT);
    }

    void updateDrawCanvas() {
        GraphicsContext graphics = drawCanvas.get().getGraphicsContext2D();
        final var scene = appManager.getScene();
        graphics.clearRect(0, 0, scene.getWidth(), scene.getHeight());
        graphics.setStroke(appManager.getColorController().getSpecPathColor());
        graphics.setTextAlign(TextAlignment.CENTER);
        graphics.setFont(new Font("", FONT_SIZE_DRAW_ANNOUNCEMENTS));
        graphics.strokeText("Fourier Drawer\nDraw something in one stroke... ;)",
                scene.getWidth() / 2., FONT_SIZE_DRAW_ANNOUNCEMENTS * 2.);
    }

    public void getNewDrawCanvas() {
        appManager.getTimeline().stop();
        Canvas canvas = drawCanvas.get();
        if (canvas != null) {
            drawRoot.getChildren().remove(canvas);
        }
        canvas = new Canvas();
        drawCanvas.set(canvas);
        canvas.widthProperty().bind(appManager.getScene().widthProperty());
        canvas.heightProperty().bind(appManager.getScene().heightProperty());
        drawRoot.getChildren().add(canvas);
        drawRoot.setBackground(new Background(new BackgroundFill(
                appManager.getColorController().getBackgroundColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        updateDrawCanvas();
        appManager.getPrimaryStage().getScene().setRoot(drawRoot);
        listenForUserInputDrawing(canvas);
    }

    private void listenForUserInputDrawing(Canvas canvas) {
        List<Point2D> drawList = new ArrayList<>();
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        canvas.setOnMouseDragged(e -> onMouseDragged(graphics, drawList, e));
        canvas.setOnMouseReleased(e -> switchBackToMainScene(drawList));
    }

    private void onMouseDragged(GraphicsContext graphics, List<Point2D> drawList, MouseEvent event) {
        graphics.lineTo(event.getX(), event.getY());
        graphics.stroke();
        final var posOrigin = appManager.getAnimationVars().getPosOrigin();
        drawList.add(new Point2D(event.getX() - posOrigin.getX(), event.getY() - posOrigin.getY()));
    }

    private void switchBackToMainScene(List<Point2D> drawList) {
        if (!drawList.isEmpty()) {
            userDrawCount.set(userDrawCount.get() + 1);
            InputValue userInput = getInputValue(drawList);
            storeUserInputValue(userInput);
            appManager.reset();
            appManager.getAnimationVars().setSelectedArmPartIndex(min(AnimationVars.START_ARM_PART_INDEX, userInput.size() - 1));
            appManager.selectDftOutputSignal(userInput, appManager.getArmType());
        }
        appManager.getPrimaryStage().getScene().setRoot(appManager.getRoot());
        appManager.updateControls();
        appManager.getTimeline().play();
    }

    private InputValue getInputValue(List<Point2D> drawList) {
        String name = String.format("User drawing %d", userDrawCount.get());
        int size = drawList.size();
        int[] arrayX = new int[size];
        int[] arrayY = new int[size];
        for (int i = 0; i < size; i++) {
            Point2D p = drawList.get(i);
            arrayX[i] = (int) p.getX();
            arrayY[i] = (int) p.getY();
        }
        return duplicateEveryInputValue(new InputValue(name, arrayX, arrayY, 0, 0, 1), (int) (1000. / size));
    }

    public StackPane getDrawRoot() {
        return drawRoot;
    }
}
