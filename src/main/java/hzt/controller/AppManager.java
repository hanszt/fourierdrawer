package hzt.controller;

import hzt.model.EpiCycleSort;
import hzt.model.data.ArmType;
import hzt.model.data.InputValue;
import hzt.utils.AppConstants;
import hzt.utils.SignalConverterKt;
import hzt.view.AnimationVars;
import hzt.view.ColorController;
import hzt.view.FourierDrawerDoubleArm;
import hzt.view.FourierDrawerSingleArm;
import hzt.view.UserInputController;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static hzt.model.EpiCycleSort.REVERSED_AMPLITUDE;
import static hzt.model.data.InputGenerator.getRandomInputValue;
import static javafx.animation.Animation.Status.PAUSED;

public final class AppManager {
    private static final List<AppManager> instancesList = new ArrayList<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(AppManager.class);
    private static final String SHOULD_BE_STATIC_TOO = "java:S1170";
    private static int instances = 0;

    //final controllers
    private final AnimationVars animationVars = new AnimationVars();
    private final ColorController colorController = new ColorController();
    private final DrawModeController drawModeController = new DrawModeController(this);
    private final UserInputController userInputController = new UserInputController(
            this, colorController, drawModeController);

    private final ArmType singleArm = new ArmType("1. Single Arm", new FourierDrawerSingleArm(
            this, animationVars, userInputController, colorController, SignalConverterKt::initializeSingleArm));
    private final ArmType singleArmApacheFft = new ArmType("2. Single Arm (Apache Fast Fourier)", new FourierDrawerSingleArm(
            this, animationVars, userInputController, colorController, SignalConverterKt::initializeFastFourierApacheCommons));

    private final ArmType singleArmFft = new ArmType("3. Single Arm (Fast Fourier)", new FourierDrawerSingleArm(
            this, animationVars, userInputController, colorController, SignalConverterKt::initializeFastFourierOwnImpl));
    private final ArmType doubleArm = new ArmType("4. Double Arm", new FourierDrawerDoubleArm(
            this, animationVars, userInputController, colorController, (inputValue, comparator) ->
            SignalConverterKt.initializeDoubleArm(inputValue, comparator, int[]::new)));
    private final ArmType doubleArmWithNoise = new ArmType("5. Double Arm With Noise", new FourierDrawerDoubleArm(
            this, animationVars, userInputController, colorController, (inputValue, comparator) ->
            SignalConverterKt.initializeDoubleArm(inputValue, comparator, SignalConverterKt::noiseSignal)));

    private final ArmType doubleArmFft = new ArmType("6. Double Arm (Fast Fourier)", new FourierDrawerDoubleArm(
            this, animationVars, userInputController, colorController, (inputValue, comparator) ->
            SignalConverterKt.initializeDoubleArmFft(inputValue, comparator, int[]::new)));

    private final ArmType doubleArmWithNoiseFft = new ArmType("7. Double Arm With Noise (Fast Fourier)", new FourierDrawerDoubleArm(
            this, animationVars, userInputController, colorController, (inputValue, comparator) ->
            SignalConverterKt.initializeDoubleArmFft(inputValue, comparator, SignalConverterKt::noiseSignal)));

    @SuppressWarnings(SHOULD_BE_STATIC_TOO)
    // This field is not static to count the nr of instances created
    private final int instance = ++instances;
    private Point2D initPoint;

    private final ObjectProperty<ArmType> armType = new SimpleObjectProperty<>(singleArmFft);

    private long startTimeSim;
    private long runTimeSim;
    private long tStart;

    private final ObjectProperty<EpiCycleSort> epiCycleComparator = new SimpleObjectProperty<>(REVERSED_AMPLITUDE);
    private final ObjectProperty<InputValue> input = new SimpleObjectProperty<>();
    private Stage primaryStage;
    private StackPane root;
    private Canvas canvas;
    private Timeline timeline;
    private Scene scene;

    public AppManager() {
        super();
    }

    public void setupStage(Stage stage) {
        instancesList.add(this);
        primaryStage = stage;
        LOGGER.info("\ninstance {} starting...", instance);
        selectDftOutputSignal(getRandomInputValue(), armType.get());
        setupScene();
        primaryStage.setScene(scene);
        primaryStage.setTitle(String.format("%s (%d)", AppConstants.TITLE, instance));
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(400);
        primaryStage.widthProperty()
                .addListener((o, c, n) -> updateCanvas(new Point2D(n.doubleValue() / 2, getAnimationVars().getPosOrigin().getY())));
        primaryStage.heightProperty()
                .addListener((o, c, n) -> updateCanvas(new Point2D(getAnimationVars().getPosOrigin().getX(), n.doubleValue() / 2)));
        primaryStage.setOnCloseRequest(e -> printClosingTextAndRemoveInstance());
        Optional.ofNullable(getClass().getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(primaryStage.getIcons()::add);
        start();
        primaryStage.show();
        updateInstancesButton("Maximum reached");
    }

    private void updateCanvas(Point2D posOrigin) {
        getAnimationVars().setPosOrigin(posOrigin);
        userInputController.setFullScreenButtonMode(primaryStage.isFullScreen());
        if (primaryStage.getScene().getRoot().equals(drawModeController.getDrawRoot())) {
            drawModeController.updateDrawCanvas();
        }
    }

    private void updateInstancesButton(String label) {
        if (instancesList.size() >= AppConstants.MAX_NUMBER_OF_INSTANCES) {
            instancesList.forEach(m -> userInputController.limitInstances(label));
        }
    }

    private void setupScene() {
        root = new StackPane();
        scene = new Scene(root, AppConstants.INIT_SCENE_SIZE.getWidth(), AppConstants.INIT_SCENE_SIZE.getHeight());
        root.setAlignment(Pos.CENTER);
        canvas = new Canvas();
        canvas.widthProperty().bind(scene.widthProperty());
        canvas.heightProperty().bind(scene.heightProperty());
        timeline = new Timeline(new KeyFrame(Duration.seconds(1. / AppConstants.INIT_FRAME_RATE),
                event -> manageSim(canvas.getGraphicsContext2D())));
        timeline.setCycleCount(Animation.INDEFINITE);
        GridPane controlsGrid = userInputController.buildConfiguredControls();
        root.getChildren().addAll(canvas, controlsGrid);
        addResponsiveSceneMovement(scene);
    }

    public Set<ArmType> constructArmTypes() {
        return new TreeSet<>(Set.of(
                singleArm, singleArmFft, singleArmApacheFft, doubleArm, doubleArmWithNoise, doubleArmFft, doubleArmWithNoiseFft));
    }

    private void start() {
        root.setBackground(new Background(new BackgroundFill(getColorController().getBackgroundColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        LOGGER.info("\n" + AppConstants.TITLE_FX);
        LOGGER.info("Animation starting... (instance {})", instance);
        setupStartScreen(canvas.getGraphicsContext2D());
        tStart = System.nanoTime();
        timeline.play();
    }

    private void addResponsiveSceneMovement(Scene scene) {
        scene.setOnMousePressed(e -> initPoint = getInitPoint(e));
        scene.setOnMouseDragged(this::moveDrawing);
        scene.setOnScroll(userInputController::zoom);
    }

    @NotNull
    private Point2D getInitPoint(MouseEvent mouseEvent) {
        final var ofSetOrigin = getAnimationVars().getOfSetOrigin();
        return new Point2D(ofSetOrigin.getX() - mouseEvent.getX(), ofSetOrigin.getY() - mouseEvent.getY());
    }

    private void moveDrawing(MouseEvent e) {
        userInputController.getFollowButton().setActive(false);
        Point2D newOfSet = new Point2D(e.getX() + initPoint.getX(), e.getY() + initPoint.getY());
        //first update the sliders, and then the actual parameters in sim. Then there will not be an interaction!
        userInputController.getHorizontalSliderUpdater().accept(-newOfSet.getX());
        userInputController.getVerticalSliderUpdater().accept(newOfSet.getY());
        getAnimationVars().setOfSetOrigin(newOfSet);
    }

    public void updateControls() {
        GridPane controlsGrid = (GridPane) root.getChildren().get(1);
        root.getChildren().remove(controlsGrid);
        controlsGrid = userInputController.buildConfiguredControls();
        root.getChildren().add(controlsGrid);
    }

    public void selectDftOutputSignal(InputValue inputValue, ArmType type) {
        input.set(adjustInputValueForFft(inputValue));
        type.drawer().initialize(input.get(), epiCycleComparator.get());
    }

    @NotNull
    private static InputValue adjustInputValueForFft(InputValue inputValue) {
        return new InputValue(
                inputValue.name(),
                SignalConverterKt.duplicateIfSizeNotPowerOfTwo(inputValue.xArray()),
                SignalConverterKt.duplicateIfSizeNotPowerOfTwo(inputValue.yArray()),
                inputValue.transX(),
                inputValue.transY(),
                inputValue.scaleFactor());
    }

    private void setupStartScreen(GraphicsContext gc) {
        gc.setStroke(getColorController().getFinalPathColor());
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(new Font("", AppConstants.FONT_SIZE_ANNOUNCEMENTS));
        gc.strokeText(AppConstants.TITLE, scene.getWidth() * 3. / 5, scene.getHeight() / 2);
    }

    public void setTimelineRate(double frameRate) {
        timeline.setRate(frameRate);
    }

    private void manageSim(GraphicsContext graphics) {
        if (System.nanoTime() - tStart > 1.5e9) {
            if (startTimeSim == 0) {
                startTimeSim = System.nanoTime();
                graphics.setFill(getColorController().getBackgroundColor());
                graphics.fillRect(0, 0, scene.getWidth(), scene.getHeight());
                LOGGER.info("Animation started...");
            }
            armType.get().drawer().drawFourier(input.get().size(), graphics);
            runTimeSim = System.nanoTime() - startTimeSim;
        }
    }

    public void updateFrameIfPaused() {
        if (timeline.getStatus() == PAUSED) {
            manageSim(canvas.getGraphicsContext2D());
        }
    }

    public void restartSim() {
        printRestartText();
        timeline.stop();
        resetForRestart();
        start();
    }

    private void printRestartText() {
        double stopTimeSim = System.nanoTime();
        LOGGER.info("Animation of instance {} restarting...\nAnimation Runtime: {} seconds\n{}\n",
                instance, (stopTimeSim - startTimeSim) / 1e9, AppConstants.DOTTED_LINE);
    }

    private void printClosingTextAndRemoveInstance() {
        double stopTimeSim = System.nanoTime();
        instancesList.remove(this);
        updateInstancesButton("New instances");
        LOGGER.info("{}\nAnimation Runtime of instance {}: {} seconds\n{}\n", AppConstants.CLOSING_MESSAGE,
                instance, (stopTimeSim - startTimeSim) / 1e9, AppConstants.DOTTED_LINE);
    }

    private void resetForRestart() {
        selectDftOutputSignal(getRandomInputValue(), armType.get());
        updateControls();
        armType.set(singleArmFft);
        setSortingComparator(REVERSED_AMPLITUDE);
        startTimeSim = 0;
        userInputController.resetButtonSettings();
        getColorController().resetColors(userInputController);
        reset();
    }

    public void reset() {
        timeline.setRate(1);
        getAnimationVars().resetAnimation();
    }

    public ArmType getArmType() {
        return armType.get();
    }

    public void setArmType(ArmType armType) {
        this.armType.set(armType);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public StackPane getRoot() {
        return root;
    }

    public Scene getScene() {
        return scene;
    }

    public double getRunTimeSim() {
        return runTimeSim;
    }

    public InputValue getInput() {
        return input.get();
    }

    public Timeline getTimeline() {
        return timeline;
    }

    public EpiCycleSort getEpiCycleComparator() {
        return epiCycleComparator.get();
    }

    public void setSortingComparator(EpiCycleSort comparator) {
        this.epiCycleComparator.set(comparator);
    }

    public AnimationVars getAnimationVars() {
        return animationVars;
    }

    public ColorController getColorController() {
        return colorController;
    }
}
