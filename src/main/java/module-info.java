module fourier_drawer {

    requires javafx.graphics;
    requires org.jetbrains.annotations;
    requires slf4j.api;
    requires commons.math3;
    requires java.desktop;
    requires javafx.controls;
    requires kotlin.stdlib;

    exports hzt to javafx.graphics;

    opens hzt.utils;
}
